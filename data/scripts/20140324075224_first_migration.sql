-- // First migration.
-- Migration SQL that makes the change goes here.
 ALTER TABLE Callout ADD COLUMN otherCalloutInfo VARCHAR (1000);
 ALTER TABLE Client MODIFY Column otherAssult BOOLEAN ;


-- //@UNDO
-- SQL to undo the change goes here.
-- ALTER TABLE Callout DROP COLUMN otherCalloutInfo ;
