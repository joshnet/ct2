-- // create ArticleCartegory Table
-- Migration SQL that makes the change goes here.
CREATE TABLE ArticleCategory (
       id INT PRIMARY KEY AUTO_INCREMENT,
       active BOOLEAN DEFAULT TRUE,
       name VARCHAR(255)
);

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE ArticleCategory ;
