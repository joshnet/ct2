-- // add royalcommission column
-- Migration SQL that makes the change goes here.
ALTER TABLE Client ADD COLUMN royalcommission BOOLEAN ;


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE Client DROP COLUMN royalcommission ;
