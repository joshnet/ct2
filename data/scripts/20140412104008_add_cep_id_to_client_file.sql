-- // add cep_id to client file
-- Migration SQL that makes the change goes here.
ALTER TABLE ClientFile ADD COLUMN cep_id INTEGER ;


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE ClientFile DROP COLUMN cep_id ;
