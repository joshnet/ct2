-- // create file table
-- Migration SQL that makes the change goes here.
CREATE TABLE ClientFile (
   id INTEGER PRIMARY KEY AUTO_INCREMENT ,
   folder_id INTEGER NOT NULL,
   size INTEGER,
   content_type VARCHAR(255),
   filename VARCHAR(255),
   path VARCHAR(500),
   uploaded_by INTEGER,
   created_on DATETIME
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE ClientFile ;
