-- // add client and cep id to file
-- Migration SQL that makes the change goes here.
ALTER TABLE ClientFile ADD COLUMN client_id INTEGER ;

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE ClientFile DROP COLUMN client_id;
