-- // add resource_type column
-- Migration SQL that makes the change goes here.
ALTER TABLE CepCard ADD COLUMN resource_type VARCHAR(255);


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE CepCard DROP COLUMN resource_type ;
