-- // create knowledgebase table
-- Migration SQL that makes the change goes here.
CREATE TABLE article (
       id INT PRIMARY KEY AUTO_INCREMENT,
       title VARCHAR(500),
       content TEXT ,
       category_id INT,
       created_on DATETIME,
       updated_on DATETIME,
       created_by INT ,
       updated_by INT ,
       status VARCHAR(255)
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE article ;
