-- // create staff_alert table
-- Migration SQL that makes the change goes here.
CREATE TABLE staff_alert (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  alert_id INTEGER ,
  staff_id INTEGER ,
  is_acknowledged BOOLEAN DEFAULT FALSE
);
-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE staff_alert ;
