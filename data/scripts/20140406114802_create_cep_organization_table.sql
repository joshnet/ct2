-- // create cep organization table
-- Migration SQL that makes the change goes here.
CREATE TABLE CepOrganization(
       id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
       name VARCHAR(255),
       active BOOLEAN DEFAULT TRUE
);

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE CepOrganization;
