-- // create cep session plan table
-- Migration SQL that makes the change goes here.
CREATE TABLE cep_session_plan (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    cep_id INTEGER,
    name VARCHAR(500)
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE cep_session_plan ;
