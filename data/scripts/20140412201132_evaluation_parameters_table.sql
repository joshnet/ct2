-- // evaluation_parameters table
-- Migration SQL that makes the change goes here.
CREATE TABLE EvaluationParameter (
       id INTEGER PRIMARY KEY AUTO_INCREMENT,
       name VARCHAR(1000),
       use_for VARCHAR(255),
       feedback_type VARCHAR(255),
       active BOOLEAN DEFAULT TRUE
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE EvaluationParameter ;
