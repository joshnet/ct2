-- // CepEvaluation
-- Migration SQL that makes the change goes here.
CREATE TABLE CepEvaluation (
   id INTEGER PRIMARY KEY AUTO_INCREMENT,
   cep_id INTEGER ,
   question VARCHAR(1000),
   use_for VARCHAR(255),
   feedback_type VARCHAR(255),
   feedback_value VARCHAR(255)
);

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE CepEvaluation ;
