-- // add client column to card
-- Migration SQL that makes the change goes here.
ALTER TABLE CepCard ADD COLUMN client VARCHAR(255) ;


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE CepCard DROP COLUMN client ;
