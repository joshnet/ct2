-- // add email to staff
-- Migration SQL that makes the change goes here.
ALTER TABLE Staff ADD COLUMN email VARCHAR(255);


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE Staff DROP COLUMN email ;
