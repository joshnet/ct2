-- // add cepfolder_id
-- Migration SQL that makes the change goes here.
ALTER TABLE ClientFile ADD COLUMN cepfolder_id INTEGER ;


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE ClientFile DROP COLUMN cepfolder_id ;
