-- // CepProjectNote
-- Migration SQL that makes the change goes here.
CREATE TABLE CepProjectNote (
       id INTEGER PRIMARY KEY AUTO_INCREMENT,
       cep_id INTEGER,
       note VARCHAR(1000),
       created_by INTEGER ,
       created_on DATETIME
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE CepProjectNote ;
