-- // add folder table
-- Migration SQL that makes the change goes here.
CREATE TABLE Folder (
     id INTEGER PRIMARY KEY AUTO_INCREMENT,
     active BOOLEAN DEFAULT TRUE,
     name CHARACTER VARYING (255)
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE Folder ;
