-- // create cep_topic table
-- Migration SQL that makes the change goes here.
CREATE TABLE cep_topic (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    cep_session_id INTEGER,
    topic VARCHAR(500),
    aim VARCHAR(500),
    resource VARCHAR(500),
    duration INTEGER
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE cep_topic ;
