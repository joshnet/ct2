-- // add purpose to ClientFile
-- Migration SQL that makes the change goes here.
ALTER TABLE ClientFile ADD COLUMN purpose VARCHAR(255);


-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE ClientFile DROP COLUMN purpose ;
