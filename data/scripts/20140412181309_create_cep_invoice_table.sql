-- // create cep invoice table
-- Migration SQL that makes the change goes here.
CREATE TABLE CepInvoice (
       id INTEGER PRIMARY KEY AUTO_INCREMENT,
       cep_id INTEGER,
       organization VARCHAR(255),
       contact_name VARCHAR(255),
       contact_email VARCHAR(255),
       contact_phone VARCHAR(255),
       note VARCHAR(1000)
);


-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE CepInvoice ;
