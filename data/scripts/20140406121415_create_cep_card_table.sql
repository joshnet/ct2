-- // create cep card table
-- Migration SQL that makes the change goes here.
CREATE TABLE CepCard (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  card_type VARCHAR(255),
  cep_organization_id INTEGER,
  contact_name VARCHAR (255),
  contact_phone VARCHAR (255),
  contact_email VARCHAR (255),
  address VARCHAR (1000),
  comments VARCHAR (5000),
  resource_name VARCHAR (1000),
  author_name VARCHAR (1000),
  client_id INTEGER ,
  client_state VARCHAR (255),
  client_suburb VARCHAR (255),
  client_postcode VARCHAR (255),
  client_phone VARCHAR (255),
  service_type VARCHAR (255),
  number_of_participants VARCHAR (255),
  fee_charged BOOLEAN DEFAULT FALSE,
  cep_invoice_id INTEGER,
  requested_date VARCHAR (500),
  requested_time VARCHAR (255),
  status VARCHAR (255),
  created_on DATETIME,
  active BOOLEAN DEFAULT TRUE
);

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE CepCard ;
