(defproject ct2 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [compojure "1.1.6"]
                 [org.clojure/java.jdbc "0.3.0"]
                 [mysql/mysql-connector-java "5.1.19"]
                 [cheshire "5.0.1"]
                 [de.ubercode.clostache/clostache "1.3.1"]
                 [crypto-password "0.1.3"]
                 [selmer "0.6.6"]
                 [com.draines/postal "1.11.1"]
                 [stuarth/clj-oauth2 "0.3.2"]
                 [clj-pdf "1.11.19"]]
  :plugins [[lein-ring "0.8.8"]]
  :ring {:handler ct2.handler/app :port 3333}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})
