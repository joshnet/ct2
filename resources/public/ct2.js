var states = [ 'ACT', 'NSW', 'VIC', 'QLD', 'SA', 'WA', 'NT', 'TAS', 'Overseas' ];

var alertSuccess = function (message) {
    $().toastmessage("showSuccessToast", message);
};

var alertError = function (message) {
    $().toastmessage("showErrorToast", message);
};

var findRecord = function (collection, id) {
    var record = null;
    for (var i = 0; i < collection.length; i++) {
        if (collection[i].id == id) {
            record = collection[i];
        }
    }
    return record;

};

var ct = angular.module("ct", [ "ngCookies", "ngRoute","ngCkeditor"]);



ct.directive("onEnter", [ "$parse", function ($parse) {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            el.on("keyup", function (event) {
                if (event.keyCode == 13) {
                    scope[attr.onEnter]();
                }
            });
        }
    };
} ]);


ct.directive("uploadForm", [function () {
    return {
        restrict: "A",
        link: function (scope, el, attrs) {
            $(el).submit(function () {
                $(this).ajaxSubmit({
                    success: function (response) {
                        $(el).find('input[type=file]').val('');
                        scope[attrs.onupload](response);
                    }
                });
                $(el).val("");
                return false;
            });
        }
    };
}]);

ct.directive("fixedPhone", [function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            $(el).mask("99 9999 9999");
        }
    };
}]);

ct.directive("mobilePhone", [function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            $(el).mask("9999 999 999");
        }
    };
}]);

ct.directive("postcode", [function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            $(el).mask("9999");
        }
    };
}]);

ct.directive("mainMenu", [ "$cookieStore", "$location", "userService", function ($cookieStore, $location, userService) {
    return {
        restrict: "E",
        templateUrl: "/views/mainmenu.html",
        link: function (scope, el, attr) {
            scope.logout = function () {
                userService.logout(function () {
                    $cookieStore.remove("userInfo");
                    $location.path("/");
                }, function (message) {
                    alertError(message);
                });

            };
            scope.changePassword = function () {
                $("#changePasswordDialog").modal({
                    show: true,
                    backdrop: 'static',
                    keyboard: 'false'
                });
            };
            scope.isUserLoggedIn = function () {
                var userInfo = $cookieStore.get("userInfo");
                if (userInfo) {
                    return true;
                }
                return false;
            };
            scope.getUserInfo = function () {
                return $cookieStore.get("userInfo");
            };
        }
    };
} ]);

ct.directive("changePasswordDialog", [ "userService", function (userService) {
    return {
        restrict: "E",
        templateUrl: "/views/changepassworddialog.html",
        scope: {},
        link: function (scope, el, attr) {
            scope.passwords = {};
            scope.closeDialog = function () {
                scope.passwords = {};
                $("#changePasswordDialog").modal('hide');
            };

            scope.changePassword = function () {
                userService.changePassword(scope.passwords, function () {
                    scope.passwords = {};
                    $("#changePasswordDialog").modal('hide');
                    alertSuccess("Password changed.");
                }, function (message) {
                    alertError(message);
                });
            };
        }
    };
} ]);

ct.directive("datemask", [ function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            $(el).inputmask('d/m/y');
        }
    };
} ]);

ct.directive("datepicker", [ function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
            $(el).attr("data-date-format", "DD/MM/YYYY");
            $(el).datetimepicker({
                pickTime: false
            });
        }
    };
} ]);

ct.directive("timepicker", [ function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            $(el).datetimepicker({
                pickDate: false
            });
        }
    };
} ]);

ct.config([ "$routeProvider", "$locationProvider",
    function ($routeProvider, $locationProvider) {
        // $locationProvider.hashPrefix("!");
        $routeProvider.when("/", {
            templateUrl: "views/login.html",
            controller: LoginCtrl,
            name: "login"
        }).when("/staticinfo", {
            templateUrl: "views/staticinfo.html",
            controller: StaticInfoCtrl,
            name: "staticinfo"
        }).when("/clients", {
            templateUrl: "views/client-list.html",
            controller: ClientsCtrl,
            name: "clients"
        }).when("/clients/:clientID", {
            templateUrl: "views/client.html",
            controller: ClientCtrl,
            name: "clients"
        }).when("/cep", {
            templateUrl: "views/cep.html",
            controller: CepCtrl,
            name: "cep"
        }).when("/article", {
            templateUrl: "views/article-list.html",
            controller: ArticlesCtrl,
            name:"article"
        }).when("/article/:id", {
            templateUrl: "views/article.html",
            controller: ArticleCtrl,
            name:"article"
        }).when("/users", {
            templateUrl:"views/users.html",
            controller:UserCtrl,
            name:"users"
        }).otherwise({
            redirectTo: "/clients"
        });
    } ]);

ct.run([ "$rootScope", "$cookieStore", "$location", "$templateCache", "$timeout", "clientService", function ($rootScope, $cookieStore, $location, $templateCache, $timeout, clientService) {

    var authSession = $cookieStore.get("authsession");
    console.log(authSession);
    if(authSession){
        $cookieStore.put("userInfo", authSession);
        window.location.href = "/";
        $cookieStore.remove("authsession");
    }

    console.log("USER INFO ", $cookieStore.get("userInfo"));

    $rootScope.session = {};
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        var userInfo = $cookieStore.get("userInfo");
        if (!userInfo) {
            if (next.name != "login") {
                $location.path("/");
            }
        }
        $rootScope.session.currentPage = next.name;
    });

    $rootScope.$on('$viewContentLoaded', function () {
        //$templateCache.removeAll();
    });

    var timer = null;
    $rootScope.loadAllMyAlerts = function () {
        clientService.loadAllMyAlerts(function (alerts) {
            if (timer) {
                $timeout.cancel(timer);
            }
            startTimer();
            $rootScope.myAlerts = alerts;
        });
    };

    $rootScope.loadAllMyAlerts();
    var startTimer = function(){
        timer = $timeout(function () {
            $rootScope.loadAllMyAlerts();
        }, 1000*60);
    };
    startTimer();

    $rootScope.$on("$destroy", function () {
        if (timer){
            $timeout.cancel(timer);
        }
    });

}]);


ct.factory("ajax", [ "$http", "$cookieStore", "$location", function ($http, $cookieStore, $location) {
    return {
        doPost: function (url, data, successCb, errorCb) {
            var userInfo = $cookieStore.get("userInfo");
            if (userInfo) {
                data.sessionKey = userInfo.sessionKey;
            }
            ;

            $http.post(url, data).success(function (response) {
                if (response.success) {
                    successCb(response.data);
                } else {
                    console.log("ERROR:" + response.message);
                    if (response.message === "Invalid session key") {
                        $cookieStore.remove("userInfo");
                        $location.path("/");
                        return;
                    }
                    if (errorCb) {
                        errorCb(response.message);
                    } else {
                        alertError(response.message);
                    }
                }
            }).error(function (httpError) {
                console.log(httpError);
            });
        }
    };
} ]);

ct.factory("userService", ["$http", "$location", "$cookieStore", "ajax", function ($http, $location, $cookieStore, ajax) {

    return {
        login: function (data, successCb, errorCb) {
            ajax.doPost("/api/user/login", data, successCb, errorCb);
        },

        logout: function (successCb, errorCb) {
            ajax.doPost("/api/user/logout", {}, successCb, errorCb);
        },

        changePassword: function (data, successCb, errorCb) {
            ajax.doPost("/api/user/changepassword", data, successCb,
                errorCb);
        },
        loadRecords: function (data, successCb, errorCb) {
            ajax.doPost("/api/user/loadall", data, successCb, errorCb);
        },
        search: function(query, successCb, errorCb) {
            ajax.doPost("/api/user/search", {query: query}, successCb, errorCb);
        },
        save: function(user, successCb, errorCb) {
            ajax.doPost("/api/user/save", user, successCb, errorCb);
        },
        resetPassword: function(userID, successCb, errorCb) {
            ajax.doPost("/api/user/resetpassword", {userID:userID}, successCb,errorCb);
        },
        resetPasswordByEmail: function(email, successCb, errorCb) {
            ajax.doPost("/api/user/resetpasswordbyemail", {email:email}, successCb,errorCb);
        }
    };
} ]);

ct.factory("staticInfoService", ["ajax", function (ajax) {
    return {
        loadRecords: function (recordType, successCb, errorCb, activeOnly) {
            ajax.doPost("/api/record/loadall", {
                recordType: recordType,
                activeOnly: activeOnly
            }, successCb, errorCb);
        },
        saveRecord: function (data, successCb, errorCb) {
            ajax.doPost("/api/record/create", data, successCb, errorCb);
        },

        deleteRecord: function (data, successCb, errorCb) {
            ajax.doPost("/api/record/delete", data, successCb, errorCb);
        },

        loadCasePlanGoalMajorCategories: function (successCb, errorCb) {
            ajax.doPost("/api/record/loadcaseplangoalmajorcategories", {}, successCb, errorCb);
        },

        loadCasePlanGoalMinorCategories: function (majorCategoryID, successCb, errorCb) {
            ajax.doPost("/api/record/loadcaseplangoalminorcategories", {
                majorCategoryID: majorCategoryID
            }, successCb, errorCb);
        }
    };
} ]);

ct.factory("attachmentService", ["ajax", function (ajax) {
    return {
        loadClientAttachments: function (data, successCb, errorCb) {
            ajax.doPost("/api/attachment/loadforclient", data, successCb, errorCb);
        },
        loadCepAttachments: function (data, successCb, errorCb) {
            ajax.doPost("/api/attachment/loadforcep", data, successCb, errorCb);
        },
        deleteAttachment: function (data, successCb, errorCb) {
            ajax.doPost("/api/attachment/delete", data, successCb, errorCb);
        }
    };
}]);

ct.factory("clientService", [ "ajax", function (ajax) {
    return {
        saveClient: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/save", data, successCb, errorCb);
        },
        loadClients: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/load", data, successCb, errorCb);
        },
        advancedSearch: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/advancedsearch", data, successCb, errorCb);
        },
        loadClientByID: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/load-by-id", data, successCb, errorCb);
        },
        deleteClient: function (clientID, successCb, errorCb) {
            ajax.doPost("/api/client/delete", {clientID: clientID}, successCb, errorCb);
        },
        mergeClients: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/merge", data, successCb, errorCb);
        },
        loadAlerts: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/loadalerts", data, successCb, errorCb);
        },
        saveAlert: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savealert", data, successCb, errorCb);
        },
        closeAlert: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/closealert", data, successCb, errorCb);
        },
        reopenAlert: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/reopenalert", data, successCb, errorCb);
        },
        acknowledgeAlert: function (alertID, successCb, errorCb) {
            ajax.doPost("/api/client/acknowledgealert", {alertID: alertID}, successCb, errorCb);
        },
        loadMyAlerts: function (clientID, successCb, errorCb) {
            ajax.doPost("/api/client/loadmyalerts", {clientID: clientID}, successCb, errorCb);
        },
        loadAllMyAlerts: function (successCb, errorCb) {
            ajax.doPost("/api/client/loadallmyalerts", {}, successCb, errorCb);
        },
        loadCaseNotes: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/loadcasenotes", data, successCb, errorCb);
        },
        saveCaseNote: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savecasenote", data, successCb, errorCb);
        },
        deleteCaseNote: function(data, successCb, errorCb){
            ajax.doPost("/api/client/deletecasenote", data, successCb, errorCb);
        },
        loadCallout: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/loadcallout", data, successCb, errorCb);
        },
        loadCallouts: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/loadcallouts", data, successCb, errorCb);
        },
        saveCallout: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savecallout", data, successCb, errorCb);
        },
        loadTrauma: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/loadtrauma", data, successCb, errorCb);
        },
        saveTrauma: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savetrauma", data, successCb, errorCb);
        },
        loadCasePlans: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/loadcaseplans", data, successCb, errorCb);
        },
        saveCaseplan: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savecaseplan", data, successCb, errorCb);
        },

        saveCaseplanGoal: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savecaseplangoal", data, successCb, errorCb);
        },

        loadCaseplanGoals: function (caseplanID, successCb, errorCb) {
            ajax.doPost("/api/client/loadcaseplangoals", {caseplanID: caseplanID}, successCb, errorCb);
        },

        saveCaseplanReview: function (data, successCb, errorCb) {
            ajax.doPost("/api/client/savecaseplanreview", data, successCb, errorCb);
        },

        loadCaseplanReviews: function (caseplanID, successCb, errorCb) {
            ajax.doPost("/api/client/loadcaseplanreviews", {caseplanID: caseplanID}, successCb, errorCb);
        },
        removeCaseplanCaseNote: function (caseplanID, casenoteID, successCb, errorCb) {
            ajax.doPost("/api/client/removecaseplancasenote", {
                caseplanID: caseplanID,
                casenoteID: casenoteID
            }, successCb, errorCb);
        },
        addCaseplanCaseNote: function (caseplanID, casenoteID, successCb, errorCb) {
            ajax.doPost("/api/client/addcaseplancasenote", {
                caseplanID: caseplanID,
                casenoteID: casenoteID
            }, successCb, errorCb);
        },
        loadCaseplanCaseNotes: function (caseplanID, successCb, errorCb) {
            ajax.doPost("/api/client/loadcaseplancasenotes", {caseplanID: caseplanID}, successCb, errorCb);
        },
        loadOpenCaseNotes: function (clientID, caseplanID, successCb, errorCb) {
            ajax.doPost("/api/client/loadopencasenotes", {clientID: clientID, caseplanID: caseplanID}, successCb, errorCb);
        }
    };
} ]);

ct.factory("articleService", ["ajax", function(ajax){
    return {
        saveArticle: function(article, successCb, errorCb){
            ajax.doPost("/api/article/save", article, successCb, errorCb);
        },
        deleteArticle: function(articleID, successCb, errorCb){
            ajax.doPost("/api/article/delete", {articleID: articleID}, successCb, errorCb);
        },
        loadByID: function(articleID, successCb, errorCb){
            ajax.doPost("/api/article/loadbyid", {articleID:articleID}, successCb, errorCb);
        },
        search: function(searchParams, successCb, errorCb){
            ajax.doPost("/api/article/search", searchParams, successCb, errorCb);
        }
    };
}]);

ct.factory("cepService", ["ajax", function (ajax) {
    return {
        saveCard: function (card, successCb, errorCb) {
            ajax.doPost("/api/cep/savecard", card, successCb, errorCb);
        },
        loadCards: function (params, successCb, errorCb) {
            ajax.doPost("/api/cep/loadcards", params, successCb, errorCb);
        },
        loadNotes: function (cepID, successCb, errorCb) {
            ajax.doPost("/api/cep/loadnotes", {cepID: cepID}, successCb, errorCb);
        },
        saveNote: function (data, successCb, errorCb) {
            ajax.doPost("/api/cep/savenote", data, successCb, errorCb);
        },
        saveInvoice: function (data, successCb, errorCb) {
            ajax.doPost("/api/cep/saveinvoice", data, successCb, errorCb);
        },
        loadInvoice: function (cepID, successCb, errorCb) {
            ajax.doPost("/api/cep/loadinvoice", {cepID: cepID}, successCb, errorCb);
        },
        saveEvaluations: function (data, successCb, errorCb) {
            ajax.doPost("/api/cep/saveevaluations", data, successCb, errorCb);
        },
        loadEvaluations: function (cepID, successCb, errorCb) {
            ajax.doPost("/api/cep/loadevaluations", {cepID: cepID}, successCb, errorCb);
        },
        saveSessionPlan: function (sessionPlan, successCb, errorCb) {
            ajax.doPost("/api/cep/savesessionplan", sessionPlan, successCb, errorCb);
        },
        loadSessionPlanByCepID: function (cepID, successCb, errorCb) {
            ajax.doPost("/api/cep/loadsessionplan", {cepID: cepID}, successCb, errorCb);
        },
        loadTopicBySessionPlanID: function (sessionPlanID, successCb, errorCb) {
            ajax.doPost("/api/cep/loadtopics", {sessionPlanID: sessionPlanID}, successCb, errorCb);
        },
        saveTopic: function (topic, successCb, errorCb) {
            ajax.doPost("/api/cep/savetopic", topic, successCb, errorCb);
        },
        deleteTopic: function (topicID, successCb, errorCb) {
            ajax.doPost("/api/cep/deletetopic", {topicID: topicID}, successCb, errorCb);
        }
    };
}]);

var LoginCtrl = [ "$scope", "$cookieStore", "$location", "userService",
    function ($scope, $cookieStore, $location, userService) {

        $scope.userView = {showLogin:true};
        if ($cookieStore.get("userInfo")) {
            $location.path("/clients");
        }

        $scope.user = {};
        $scope.login = function () {
            userService.login($scope.user, function (userInfo) {
                $cookieStore.put("userInfo", userInfo);
                $scope.user = {};

       $location.path("/clients");
            }, function (message) {
                alertError(message);
            });
        };

        $scope.getUserInfo = function () {
            return $cookieStore.get("userInfo");
        };

        $scope.resetPassword = function(email){
            userService.resetPasswordByEmail(email, function(){
                alertSuccess("Password has been reset. Please check the new password in your email.");
                $scope.userView.showLogin = true;
            });
        };
    }];

var StaticInfoCtrl = [
    "$scope",
    "$routeParams",
    "staticInfoService",
    function ($scope, $routeParams, staticInfoService) {

        $scope.staticRecordTypes = [
            {
                description: "Alert Reasons",
                name: "AlertReason"
            },
            {
                description: "Alert Types",
                name: "AlertType"
            },
            {
                description: "Casenote Contact Types",
                name: "CaseNoteContactType"
            },
            {
                description: "Casenote Outcomes",
                name: "CaseNoteOutcome"
            },
            {
                description: "Casenote Reasons",
                name: "CaseNoteReason"
            },
            {
                description: "Caseplan Closure Categories",
                name: "CasePlanClosureCategory"
            },
            {
                description: "Client Statuses",
                name: "ClientStatus"
            },
            {
                description: "Cultural Identities",
                name: "CulturalIdentity"
            },
            {
                description: "Cultures",
                name: "Culture"
            },
            {
                description: "Ethnicities",
                name: "Ethnicity"
            },
            {
                description: "Genders",
                name: "IdentifiedGender"
            },
            {
                description: "Offender Relationships",
                name: "OffenderRelationship"
            },
            {
                description: "Sexualities",
                name: "Sexuality"
            },
            {
                description: "Therapy Methods",
                name: "TherapyMethod"
            },
            {
                description: "Training Types",
                name: "TrainingType"
            },
            {
                description: "Homlessness Risks",
                name: "HomelessRisk"
            },
            {
                description: "Drug Uses",
                name: "DrugUse"
            },
            {
                description: "Caseplan Goal Categories",
                name: "CasePlanGoalCategory"
            },
            {
                description: "Client Folders",
                name: "Folder"
            },
            {
                description: "CEP Organizations",
                name: "CepOrganization"
            },
            {
                description: "CEP Folders",
                name: "CepFolder"
            },
            {
                description: "Evaluation Parameters",
                name: "EvaluationParameter"
            },{
                description:"Article Categories",
                name:"ArticleCategory"
            }
        ];

        $scope.setCurrentRecordType = function (recordType) {
            $scope.currentRecordType = recordType;
            $scope.loadRecords();
        };

        $scope.records = [];

        $scope.loadRecords = function () {
            if ($scope.currentRecordType) {
                if ($scope.currentRecordType.name == "CasePlanGoalCategory") {
                    staticInfoService
                        .loadCasePlanGoalMajorCategories(
                        function (records) {
                            $scope.records = records;
                            if ($scope.currentCasePlanGoalMajorCategory) {
                                $scope
                                    .loadCasePlanGoalMinorCategories();
                            }
                        }, function (message) {
                            alertError(message);
                        });
                } else {

                    if ($scope.currentRecordType) {
                        staticInfoService.loadRecords(
                            $scope.currentRecordType.name, function (records) {
                                $scope.records = records;
                            }, function (message) {
                                alertError(message);
                            }, false);
                    }
                }
            }
        };
        $scope.loadRecords();

        $scope.casePlanGoalMinorCategories = [];
        $scope.loadCasePlanGoalMinorCategories = function () {
            staticInfoService.loadCasePlanGoalMinorCategories(
                $scope.currentCasePlanGoalMajorCategory.id, function (records) {
                    $scope.casePlanGoalMinorCategories = records;
                }, function (message) {
                    alertError(message);
                });
        };

        $scope.currentRecord = {};

        $scope.currentCasePlanGoalMajorCategory = null;

        $scope.createNewRecord = function () {
            $scope.currentRecord = {};
            $scope.showRecordModal = true;
        };

        $scope.createNewCasePlanGoalCategory = function () {
            $scope.currentRecord = {};
            $scope.showCasePlanGoalCategoryModal = true;
        };

        $scope.selectRecord = function (record) {
            $scope.currentRecord = record;
            $scope.currentCasePlanGoalMajorCategory = record;
            $scope.loadCasePlanGoalMinorCategories(record.id);
        };

        $scope.saveRecord = function () {
            var postData = {};
            if (!$scope.currentRecord.active) {
                $scope.currentRecord.active = false;
            }
            if ($scope.currentRecord.id) {
                postData.id = $scope.currentRecord.id;
                delete $scope.currentRecord.id;
            }
            if ($scope.currentRecord.majorCategory) {
                $scope.currentRecord.majorCategory_id = $scope.currentRecord.majorCategory.id;
                delete $scope.currentRecord.majorCategory;
            }
            postData.recordType = $scope.currentRecordType.name;
            postData.record = $scope.currentRecord;
            staticInfoService.saveRecord(postData, function () {
                alertSuccess("Record saved");
                $scope.cancelRecord();
                $scope.cancelCasePlanGoalCategory();
                $scope.loadRecords();
            }, function (message) {
                alertError(message);
            });
        };

        $scope.cancelRecord = function () {
            $scope.currentRecord = {};
            $scope.showRecordModal = false;
            $scope.loadRecords();
        };

        $scope.cancelCasePlanGoalCategory = function () {
            $scope.currentRecord = {};
            $scope.showCasePlanGoalCategoryModal = false;
            $scope.loadRecords();
        };

        $scope.deleteRecord = function (record) {
            if (window.confirm("Are you sure you want to delete '"
                + record.name + "'?")) {
                staticInfoService.deleteRecord({
                    recordType: $scope.currentRecordType.name,
                    id: record.id
                }, function () {
                    alertSuccess("Record deleted.");
                    $scope.loadRecords();
                }, function (message) {
                    alertError(message);
                });
            }
        };

        $scope.editRecord = function (record) {
            $scope.currentRecord = record;
            $scope.showRecordModal = true;
        };

        $scope.editCasePlanGoalCategory = function (record) {
            $scope.currentRecord = record;
            if (record.majorcategory_id) {
                $scope.currentRecord.majorCategory = getMajorCategory(record.majorcategory_id);
            }
            $scope.showCasePlanGoalCategoryModal = true;
        };

        var getMajorCategory = function (id) {
            var result = null;
            angular.forEach($scope.records, function (record) {
                if (record.id == id) {
                    result = record;
                }
            });
            return result;
        };

    } ];

var ClientsCtrl = [
    "$scope",
    "$routeParams",
    "$location",
    "$cookieStore",
    "clientService",
    "userService",
    function ($scope, $routeParams, $location,$cookieStore, clientService, userService) {

        $scope.clientView = {};


        $scope.users = [] ;

        $scope.loadUsers = function(){
            userService.loadRecords({}, function(data){
                $scope.users = data ;
            });
        };

        $scope.loadUsers();

        $scope.newClient = {};
        $scope.createNewClient = function () {
            $scope.showNewClientModal = true;
            $scope.newClient = {};
        };

        $scope.cancelNewClient = function () {
            $scope.newClient = {};
            $scope.showNewClientModal = false;
        };

        $scope.saveNewClient = function () {
            clientService.saveClient($scope.newClient, function () {
                alertSuccess("Client saved.");
                $scope.showNewClientModal = false;
                $scope.newClient = {};
            }, function (message) {
                alertError(message);
            });
        };

        $scope.queryParams = {};
        $scope.queryParams.substring = $routeParams.substring || "";

        $scope.queryParams.mineOnly = $routeParams.mine;
        $scope.queryParams.samssaOnly = $routeParams.samssa;
        $scope.clients = [];
        $scope.recordCount = 0;
        $scope.queryParams.limit = 25;
        $scope.queryParams.offset = ($routeParams.page - 1)
            * $scope.queryParams.limit || 0;

        $scope.clientView.advancedSearch = false;

        $scope.loadClients = function () {
            if($scope.clientView.advancedSearch){
                clientService.advancedSearch($scope.queryParams, function (result) {
                    $scope.clients = result.records;
                    $scope.recordCount = result.count;
                }, function (message) {
                    alertError(message);
                });
            }else{
                clientService.loadClients($scope.queryParams, function (result) {
                    $scope.clients = result.records;
                    $scope.recordCount = result.count;
                }, function (message) {
                    alertError(message);
                });
            }

        };
        $scope.loadClients();

        // paging stuff
        $scope.currentPage = $routeParams.page || 1;
        $scope.getPages = function () {
            var pages = [];
            if ($scope.recordCount == 0) {
                return [];
            }
            for (var i = 1; i < Math.ceil($scope.recordCount
                / $scope.queryParams.limit) + 1; i++) {
                pages.push(i);
            }
            return pages;
        };

        $scope.movePrevious = function () {
            if ($scope.currentPage > 1) {
                $scope.goToPage($scope.currentPage - 1);
            }
        };

        $scope.moveNext = function () {
            if ($scope.currentPage < Math.ceil($scope.recordCount / $scope.queryParams.limit)) {
                $scope.goToPage($scope.currentPage + 1);
            };
        };

        $scope.loadPage = function () {
            var urlParams = {};
            if ($scope.currentPage) {
                urlParams.page = $scope.currentPage;
            }
            ;
            if ($scope.queryParams.substring) {
                urlParams.substring = $scope.queryParams.substring;
            }
            ;
            if ($scope.queryParams.mineOnly) {
                urlParams.mine = $scope.queryParams.mineOnly;
            }
            ;
            if ($scope.queryParams.samssaOnly) {
                urlParams.samssa = $scope.queryParams.samssaOnly;
            }
            ;
            $location.search(urlParams);
        };

        $scope.goToPage = function (page) {
            $scope.currentPage = page;
            $scope.loadPage();
        };

        $scope.doSearch = function () {
            $scope.currentPage = 1;
            $scope.loadPage();
        };

        $scope.simpleSearch = function(){
            $scope.clientView.advancedSearch = false;
            $scope.loadClients();
        };

        $scope.selectClient = function (client) {
            $location.path("/clients/" + client.id).search("tab", "summary");
        };

        $scope.getUserInfo = function(){
            return $cookieStore.get("userInfo");
        };

        if($scope.getUserInfo().role==1){
            $scope.filterProps = ["ID","firstname", "lastname", "othername", "dateofbirth", "filenumber", "counsellor","group"];
        }else{
            $scope.filterProps = ["ID","firstname", "lastname", "othername", "dateofbirth", "filenumber","group"];
        }
        $scope.filterProps = [];
        $scope.groups = ["Nguru", "SAMSSA", "RoyalCommission", "CRCC"];
        $scope.clientView = {} ;

        //for advanced search
        $scope.queryParams.params = [] ;
        $scope.currentCriteria = {};

        $scope.addCriteria = function(){
            $scope.queryParams.params.push($scope.currentCriteria);
            $scope.currentCriteria = {};
        };

        $scope.removeCriteria = function(prop){
            if(prop == 'counsellor'){
                var prop = $scope.currentCriteria.prop ;
                var op = $scope.currentCriteria.op ;
                var val = $scope.currentCriteria.value;
                $scope.currentCriteria = {prop:prop, op:op, value:val};
                $scope.queryParams.counsellors = null ;
                return ;
            }
            if(prop == 'group'){
                var prop = $scope.currentCriteria.prop ;
                var op = $scope.currentCriteria.op ;
                var val = $scope.currentCriteria.value;
                $scope.currentCriteria = {prop:prop, op:op, value:val};
                $scope.queryParams.groups = null ;
                return ;
            }
            var newParams = [] ;
            angular.forEach($scope.queryParams.params, function(p){
                if(!(p.prop === prop)){
                    newParams.push(p);
                }
            });
            $scope.queryParams.params = newParams;
        };

        var groupExists = function(list, group){
            var exists = false ;
            angular.forEach(list, function(g){
                if(g === group){
                    exists = true;
                }
            });
            return exists ;
        };

        var counsellorExists = function(list, counsellor){
            var exists = false ;
            angular.forEach(list, function(c){
                if(c.username === counsellor.username){
                    exists = true;
                }
            });
            return exists ;
        };

        $scope.addGroup = function(value, prop){
            if(value){
                if($scope.queryParams.groups){
                    if(!groupExists($scope.queryParams.groups, prop)){
                        $scope.queryParams.groups.push(prop);
                    }
                }else{
                    $scope.queryParams.groups = [];
                    if(!groupExists($scope.queryParams.groups, prop)){
                        $scope.queryParams.groups.push(prop);
                    }
                }
            }else{
                if($scope.queryParams.groups){
                    var groups = [] ;
                    angular.forEach($scope.queryParams.groups, function(g){
                        if(!(g === prop)){
                            groups.push(g);
                        };
                    });
                    $scope.queryParams.groups = groups;
                }
            };
            console.log($scope.queryParams.groups);
        };

        $scope.addCounsellor = function(value, counsellor){
            if(value){
                if($scope.queryParams.counsellors){
                    if(!counsellorExists($scope.queryParams.counsellors, counsellor)){
                        $scope.queryParams.counsellors.push(counsellor);
                    }
                }else{
                    $scope.queryParams.counsellors = [];
                    if(!counsellorExists($scope.queryParams.counsellors, counsellor)){
                        $scope.queryParams.counsellors.push(counsellor);
                    }
                }
            }else{
                if($scope.queryParams.counsellors){
                    var counsellors = [] ;
                    angular.forEach($scope.queryParams.counsellors, function(c){
                        if(!(c.username === counsellor.username)){
                            counsellors.push(c);
                        };
                    });
                    $scope.queryParams.counsellors = counsellors;
                }
            };
        };

        $scope.advancedSearch = function(){
            var groups = $scope.queryParams.groups ;
            var counsellors = $scope.queryParams.counsellors;
            var params = $scope.queryParams.params ;
            var newParams = [] ;

            //remove all the 'is' ops because they were added dynamically
            angular.forEach(params, function(p){
                if(!(p.op === 'is')){
                    newParams.push(p);
                };
            });
            $scope.queryParams.params = newParams ;
            angular.forEach(groups, function(g){
                $scope.queryParams.params.push({prop:g, op:"is", value: 1});
            });

            var userIDs = [];
            angular.forEach(counsellors, function(c){
                userIDs.push(c.id);
            });
            if(userIDs.length > 0) {
                $scope.queryParams.params.push({prop:'counsellor', op:'is', value:userIDs});
            };
            $scope.currentPage = 1;
            $scope.clientView.advancedSearch = true;
            $scope.loadClients();
        };
    }];

var ClientCtrl = [
    "$scope",
    "$routeParams",
    "$location",
    "clientService",
    "staticInfoService",
    "userService",
    "attachmentService",
    function ($scope, $routeParams, $location, clientService, staticInfoService, userService, attachmentService) {

        $scope.clientID = parseInt($routeParams.clientID) || 0;

        // static info
        $scope.loadStaticInfo = function () {
            $scope.identifiedGenders = [];
            staticInfoService.loadRecords("IdentifiedGender", function (records) {
                $scope.identifiedGenders = records;
                $scope.client.identifiedgender_id = findRecord(records, $scope.client.identifiedgender_id);
            });
            $scope.contactRelations = [ 'Primary victim/survivor',
                'Partner of victim/survivor',
                'Friend of victim/survivor',
                'Parent of victim/survivor',
                'Other family member of victim/survivor',
                'Carer/supporter of victim/survivor',
                'Professional/organisational representative' ];

            $scope.clientStatuses = [];
            staticInfoService.loadRecords("ClientStatus", function (records) {
                $scope.clientStatuses = records;
                $scope.client.status_id = findRecord(records, $scope.client.status_id);
            });

            $scope.staffs = [];
            userService.loadRecords({}, function (records) {
                $scope.staffs = records;
                $scope.client.counsellor_id = findRecord(records, $scope.client.counsellor_id);
            });

            $scope.homelessRisks = [];
            staticInfoService.loadRecords("HomelessRisk", function (records) {
                $scope.homelessRisks = records;
                $scope.client.homelessrisk_id = findRecord(records, $scope.client.homelessrisk_id);
            });

            $scope.cultures = [];
            staticInfoService.loadRecords("Culture", function (records) {
                $scope.cultures = records;
                $scope.client.culture_id = findRecord(records, $scope.client.culture_id);
            });

            $scope.states = states;

            $scope.ethnicities = [];
            staticInfoService.loadRecords("Ethnicity", function (records) {
                $scope.ethnicities = records;
                $scope.client.ethnicity_id = findRecord(records, $scope.client.ethnicity_id);
            });

            $scope.sexualities = [];
            staticInfoService.loadRecords("Sexuality", function (records) {
                $scope.sexualities = records;
                $scope.client.identifiedsexuality_id = findRecord(records, $scope.client.identifiedsexuality_id);
            });

            $scope.alertParams.clientID = $scope.client.id;
            $scope.loadAlerts($scope.alertParams);
            $scope.loadMyAlerts();

            $scope.alertTypes = [];
            staticInfoService.loadRecords("AlertType", function (records) {
                $scope.alertTypes = records;
            });

            $scope.alertReasons = [];
            staticInfoService.loadRecords("AlertReason", function (records) {
                $scope.alertReasons = records;
            });

            $scope.loadCaseNotes();

            staticInfoService.loadRecords('CaseNoteReason', function (records) {
                $scope.caseNoteReasons = records;
            });

            staticInfoService.loadRecords('CaseNoteOutcome', function (records) {
                $scope.caseNoteOutcomes = records;
            });

            staticInfoService.loadRecords('CaseNoteContactType', function (records) {
                $scope.caseNoteContactTypes = records;
            });

            $scope.loadCasePlans();

            staticInfoService.loadRecords('CasePlanClosureCategory', function (records) {
                $scope.casePlanClosureCategories = records;
            }, function (message) {
                alertError(message);
            });

            staticInfoService.loadRecords('TherapyMethod', function (records) {
                $scope.therapyMethods = records;
            }, function (message) {
                alertError(message);
            });

            $scope.severities = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

            staticInfoService.loadCasePlanGoalMajorCategories(function (records) {
                $scope.majorCategories = records;
                $scope.getReviewItems();
            }, function (message) {
                alertError(message);
            });

            $scope.folders = [];
            staticInfoService.loadRecords("Folder", function (folders) {
                $scope.folders = folders;
            }, function (errMsg) {
                alertError(errMsg);
            });

            $scope.deleteClient = function () {
                if (confirm("Are you sure you want to delete '" + $scope.client.firstname + " " + $scope.client.lastname + "' ? ")) {
                    clientService.deleteClient($scope.client.id, function () {
                        alertSuccess("Client Deleted!");
                        $location.path("/clients");
                    });
                }
                ;
            };

            $scope.clientView = {};
            if ($routeParams.tab) {
                $scope.clientView.tab = $routeParams.tab;
            }
            $scope.getCurrentTemplate = function () {
                return '/views/client-' + $scope.clientView.tab + '.html';
            };

            $scope.openMergeDialog = function () {
                $scope.clientView.showMergeDialog = true;
            };

            $scope.clients = [];
            $scope.loadClients = function (filter) {
                $scope.clients = [];
                clientService.loadClients({limit: 50, substring: filter}, function (result) {
                    angular.forEach(result.records, function (r) {
                        if (r.id !== $scope.client.id) {
                            $scope.clients.push(r);
                        }
                    });
                });
            };

            var getClientName = function (client) {
                return client.firstname + " " + client.lastname + ", " + client.othername;
            };

            $scope.mergeClient = function (sourceClient) {
                if (sourceClient.id == $scope.client.id) {
                    alertError("You cannot merge a client record into its self.");
                    return;
                }
                if (confirm("You are about to merge " + getClientName(sourceClient) + " into " + getClientName($scope.client) + ". Continue?")) {
                    clientService.mergeClients({
                        targetID: $scope.client.id,
                        sourceID: sourceClient.id
                    }, function () {
                        $scope.clientView.showMergeDialog = false;
                        alertSuccess("Clients Merged!");
                    });
                }
                ;
            };
        };

        $scope.calloutView = {showTable:true};
        $scope.createCallout = function(){
            $scope.createNewCallout();
            $scope.calloutView.showTable = false ;
        };

        $scope.showCalloutTable = function(){
            $scope.currentCallout = {};
            $scope.calloutView.showTable = true;
        };

        $scope.createNewCallout = function(){
            $scope.currentCallout = {};
        };

        $scope.loadCallout = function (calloutID) {
            clientService.loadCallout({
                calloutID: calloutID
            }, function (callout) {
                if (callout) {
                    $scope.currentCallout = callout;
                } else {
                    $scope.createNewCallout();
                }
            }, function (error) {
                alertError(error);
            });
        };

        $scope.selectCallout = function(calloutID){
            $scope.loadCallout(calloutID);
            $scope.calloutView.showTable = false;
        };

        $scope.callouts = [] ;

        $scope.loadCallouts = function(){
            clientService.loadCallouts({
                clientID:$scope.client.id
            },function(callouts){
                $scope.callouts = callouts;
            });
        };

        $scope.trauma = {};
        $scope.loadTrauma = function () {
            clientService.loadTrauma({
                clientID: $scope.client.id
            }, function (trauma) {
                if (trauma) {
                    $scope.trauma = trauma;
                } else {
                    $scope.trauma = {};
                }
            }, function (msg) {
                alertError(msg);
            });
        };

        $scope.caseplans = [];
        $scope.loadCasePlans = function () {
            clientService.loadCasePlans({
                clientID: $scope.client.id
            }, function (caseplans) {
                $scope.caseplans = caseplans;
            }, function (message) {
                alertError(message);
            });
        };

        if ($scope.clientID == 0) {
            $location.path("/clients");
        }

        $scope.client = null;
        $scope.loadClient = function () {
            clientService.loadClientByID({
                clientID: $scope.clientID
            }, function (client) {
                $scope.client = client;
                $scope.loadStaticInfo();
                $scope.loadCallouts();
                $scope.loadTrauma();
            }, function (errorMsg) {
                alertError(errorMsg);
            });
        };
        $scope.loadClient();

        $scope.currentView = 'summary';

        $scope.saveClient = function () {
            console.log($scope.client);
            var updatedClient = angular.copy($scope.client);
            updatedClient.identifiedgender_id = $scope.client.identifiedgender_id ? $scope.client.identifiedgender_id.id : null;
            updatedClient.status_id = $scope.client.status_id ? $scope.client.status_id.id : null;
            updatedClient.counsellor_id = $scope.client.counsellor_id ? $scope.client.counsellor_id.id : null;
            updatedClient.homelessrisk_id = $scope.client.homelessrisk_id ? $scope.client.homelessrisk_id.id : null;
            updatedClient.culture_id = $scope.client.culture_id ? $scope.client.culture_id.id : null;
            updatedClient.ethnicity_id = $scope.client.ethnicity_id ? $scope.client.ethnicity_id.id : null;
            updatedClient.identifiedsexuality_id = $scope.client.identifiedsexuality_id ? $scope.client.identifiedsexuality_id.id : null;
            clientService.saveClient(updatedClient, function (data) {
                alertSuccess("Client saved!");
                $scope.loadClient();
            }, function (message) {
                alertError(message);
            });
        };

        $scope.resetClient = function () {
            if (window.confirm("Are you sure you want to reset?")) {
                $scope.loadClient();
            }
        };

        // alerts
        $scope.alertParams = {};
        $scope.alerts = [];
        $scope.loadAlerts = function () {
            clientService.loadAlerts($scope.alertParams, function (records) {
                $scope.alerts = records;
            }, function (message) {
                alertError(message);
            });
        };

        $scope.createNewAlert = function () {
            $scope.currentAlert = {
                active: true
            };
        };

        $scope.cancelAlert = function () {
            if (window.confirm("Are you sure you want to cancel?")) {
                $scope.createNewAlert();
            }
        };

        $scope.saveAlert = function () {
            console.log($scope.currentAlert);
            var updatedAlert = angular.copy($scope.currentAlert);
            updatedAlert.alerttype_id = $scope.currentAlert.alerttype_id ? $scope.currentAlert.alerttype_id.id
                : null;
            updatedAlert.alertreason_id = $scope.currentAlert.alertreason_id ? $scope.currentAlert.alertreason_id.id
                : null;
            updatedAlert.client_id = $scope.client.id;
            clientService.saveAlert(updatedAlert, function (id) {
                alertSuccess("Alert saved!");
                $scope.loadAlerts();
                $scope.loadMyAlerts();
                $scope.loadAllMyAlerts(); //from root
                $scope.currentAlert.id = id;
            }, function (message) {
                alertError(message);
            });
        };

        $scope.closeAlert = function () {
            if(window.confirm("Are you sure you want to close this alert?")){
                var updatedAlert = angular.copy($scope.currentAlert);
                updatedAlert.alerttype_id = $scope.currentAlert.alerttype_id ? $scope.currentAlert.alerttype_id.id
                    : null;
                updatedAlert.alertreason_id = $scope.currentAlert.alertreason_id ? $scope.currentAlert.alertreason_id.id
                    : null;
                updatedAlert.client_id = $scope.client.id;
                updatedAlert.active = false ;
                clientService.closeAlert(updatedAlert, function (id) {
                    alertSuccess("Alert closed!");
                    $scope.loadAlerts();
                    $scope.loadMyAlerts();
                    $scope.loadAllMyAlerts(); //from root
                    $scope.currentAlert.id = id;
                    $scope.currentAlert.active = false ;
                }, function (message) {
                    alertError(message);
                });
            }
        };

        $scope.reopenAlert = function () {
            if(window.confirm("Are you sure you want to reopen this alert?")){
                var updatedAlert = angular.copy($scope.currentAlert);
                updatedAlert.alerttype_id = $scope.currentAlert.alerttype_id ? $scope.currentAlert.alerttype_id.id
                    : null;
                updatedAlert.alertreason_id = $scope.currentAlert.alertreason_id ? $scope.currentAlert.alertreason_id.id
                    : null;
                updatedAlert.client_id = $scope.client.id;
                updatedAlert.active = true ;
                clientService.reopenAlert(updatedAlert, function (id) {
                    alertSuccess("Alert reopened!");
                    $scope.loadAlerts();
                    $scope.loadMyAlerts();
                    $scope.loadAllMyAlerts(); //from root
                    $scope.currentAlert.id = id;
                    $scope.currentAlert.active = true;
                }, function (message) {
                    alertError(message);
                });
            }
        };

        $scope.selectAlert = function (alert) {
            $scope.currentAlert = angular.copy(alert);
            $scope.currentAlert.alerttype_id = findRecord(
                $scope.alertTypes, alert.alerttype_id);
            $scope.currentAlert.alertreason_id = findRecord(
                $scope.alertReasons, alert.alertreason_id);
            delete $scope.currentAlert.alertType;
        };

        $scope.clientPendingAlerts = [];
        $scope.loadMyAlerts = function () {
            clientService.loadMyAlerts($scope.client.id, function (alerts) {
                $scope.clientPendingAlerts = alerts;
            });
        };
        $scope.acknowledgeAlert = function (alert) {
            clientService.acknowledgeAlert(alert.id, function () {
                $scope.loadMyAlerts();
                $scope.loadAllMyAlerts(); //from root
                alertSuccess("Alert acknowledged.");
            });
        };

        // casenotes
        $scope.caseNoteParams = {};
        $scope.caseNotes = [];
        $scope.loadCaseNotes = function () {
            $scope.caseNoteParams.clientID = $scope.client.id;
            clientService.loadCaseNotes($scope.caseNoteParams, function (records) {
                $scope.caseNotes = records;
            }, function (message) {
                alertError(message);
            });
        };

        $scope.currentCaseNote = {};
        $scope.createCaseNote = function () {
            $scope.currentCaseNote = {};
            var now = new Date();
            var dateStr = now.getDate()+"/"+(now.getMonth()+1)+"/"+now.getFullYear();
            console.log(dateStr);
            $scope.currentCaseNote.dateofservice = dateStr;
        };
        $scope.cancelCaseNote = function () {
            $scope.createCaseNote();
        };
        $scope.selectCaseNote = function (caseNote) {
            $scope.currentCaseNote = angular.copy(caseNote);
            $scope.currentCaseNote.reason_id = findRecord($scope.caseNoteReasons, $scope.currentCaseNote.reason_id);
            $scope.currentCaseNote.contacttype_id = findRecord($scope.caseNoteContactTypes, $scope.currentCaseNote.contacttype_id);
            $scope.currentCaseNote.outcome_id = findRecord($scope.caseNoteOutcomes, $scope.currentCaseNote.outcome_id);
        };

        $scope.caseNoteMinutes = [ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50,
            55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115,
            120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175,
            180 ];
        $scope.caseNoteReasons = [];
        $scope.caseNoteContactTypes = [];
        $scope.saveCaseNote = function () {
            var updatedCaseNote = angular.copy($scope.currentCaseNote);
            updatedCaseNote.reason_id = $scope.currentCaseNote.reason_id ? $scope.currentCaseNote.reason_id.id : null;
            updatedCaseNote.outcome_id = $scope.currentCaseNote.outcome_id ? $scope.currentCaseNote.outcome_id.id : null;
            updatedCaseNote.contacttype_id = $scope.currentCaseNote.contacttype_id ? $scope.currentCaseNote.contacttype_id.id : null;
            updatedCaseNote.client_id = $scope.client.id;
            clientService.saveCaseNote(updatedCaseNote, function () {
                alertSuccess("Case Note saved!");
                $scope.createCaseNote();
                $scope.loadCaseNotes();
            }, function (msg) {
                alertError(msg);
            });
        };

        $scope.saveCallout = function () {
            $scope.currentCallout.client_id = $scope.client.id;
            clientService.saveCallout($scope.currentCallout, function(id) {
                $scope.loadCallouts();
                $scope.loadCallout(id);
                alertSuccess("Callout Saved!");
            }, function (msg) {
                alertError(msg);
            });
        };

        $scope.saveCalloutAndViewTable = function(){
            $scope.saveCallout();
            $scope.calloutView.showTable = true;
        };

        $scope.cancelCallout = function () {
            $scope.loadCallout($scope.currentCallout.id);
            $scope.calloutView.showTable = true;
        };

        $scope.saveTrauma = function () {
            var updatedTrauma = angular.copy($scope.trauma);
            updatedTrauma.client_id = $scope.client.id;
            clientService.saveTrauma(updatedTrauma, function () {
                alertSuccess("Record Saved!");
            }, function (errorMessage) {
                alertError(errorMessage);
            });
        };

        $scope.cancelTrauma = function () {
            $scope.loadTrauma();
        };

        $scope.currentCaseplan = {};
        $scope.createCaseplan = function () {
            $scope.caseplanView.open = true;
            $scope.currentCaseplan = {};
        };
        $scope.saveCaseplan = function () {
            var updatedCaseplan = angular.copy($scope.currentCaseplan);
            updatedCaseplan.closurecategory_id = $scope.currentCaseplan.closurecategory_id ? $scope.currentCaseplan.closurecategory_id.id : null;
            updatedCaseplan.client_id = $scope.client.id;
            clientService.saveCaseplan(updatedCaseplan, function (savedCaseplan) {
                $scope.selectCaseplan(savedCaseplan);
                alertSuccess("Caseplan saved!");
                if (!$scope.currentCaseplan.id) {
                    $scope.currentCaseplan = {};
                }
                $scope.loadCasePlans();
                $scope.caseplanView.open = false;
            }, function (message) {
                alertError(message);
            });

        };

        $scope.cancelCaseplan = function () {
            $scope.currentCaseplan = {};
            $scope.caseplanView.open = false;
        };

        $scope.caseplanView = {open: false, page: 'list', tab: 'details'};

        $scope.selectCaseplan = function (caseplan) {
            $scope.currentCaseplan = caseplan;
            $scope.currentCaseplan.closurecategory_id = findRecord($scope.casePlanClosureCategories, caseplan.closurecategory_id);
            $scope.caseplanView.page = 'detail';
            $scope.loadCaseplanGoals();
            $scope.loadCaseplanReviews();
            $scope.loadCaseplanCaseNotes();
            $scope.loadCaseplanOpenCaseNotes();
        };

        //caseplan Goal
        $scope.caseplanGoal = {};
        $scope.createCaseplanGoal = function () {
            $scope.caseplanGoal = {};
            $scope.caseplanView.showGoal = true;
        };
        $scope.cancelGoal = function () {
            $scope.caseplanGoal = {};
            $scope.caseplanView.showGoal = false;
        };
        $scope.selectCaseplanGoal = function (goal) {
            $scope.caseplanGoal = angular.copy(goal);
            $scope.caseplanGoal.therapymethod_id = findRecord($scope.therapyMethods, goal.therapymethod_id);
            $scope.caseplanGoal.majorcategory_id = findRecord($scope.majorCategories, goal.majorcategory_id);
            staticInfoService.loadCasePlanGoalMinorCategories($scope.caseplanGoal.majorcategory_id.id, function (minorCategories) {
                $scope.minorCategories = minorCategories;
                $scope.caseplanGoal.minorcategory_id = findRecord($scope.minorCategories, goal.minorcategory_id);
            }, function (message) {
                alertError(message);
            });
            $scope.caseplanView.showGoal = true;
        };

        $scope.loadCaseplanGoals = function () {
            if ($scope.currentCaseplan) {
                clientService.loadCaseplanGoals($scope.currentCaseplan.id, function (goals) {
                    $scope.caseplanGoals = goals;
                    $scope.getReviewItems();
                }, function (errorMsg) {
                    alertError(errorMsg);
                });
            }
        };

        $scope.loadCaseplanCaseNotes = function () {
            clientService.loadCaseplanCaseNotes($scope.currentCaseplan.id, function (casenotes) {
                $scope.caseplanCaseNotes = casenotes;
            }, function (errMsg) {
                alertError(errMsg);
            });
        };

        $scope.loadCaseplanOpenCaseNotes = function () {
            clientService.loadOpenCaseNotes($scope.client.id, $scope.currentCaseplan.id, function (casenotes) {
                $scope.openCaseNotes = casenotes;
            }, function (errMsg) {
                alertError(errMsg);
            });
        };

        $scope.saveGoal = function () {
            var newGoal = angular.copy($scope.caseplanGoal);
            newGoal.caseplan_id = $scope.currentCaseplan.id;
            newGoal.majorcategory_id = $scope.caseplanGoal.majorcategory_id ? $scope.caseplanGoal.majorcategory_id.id : null;
            newGoal.minorcategory_id = $scope.caseplanGoal.minorcategory_id ? $scope.caseplanGoal.minorcategory_id.id : null;
            newGoal.therapymethod_id = $scope.caseplanGoal.therapymethod_id ? $scope.caseplanGoal.therapymethod_id.id : null;
            clientService.saveCaseplanGoal(newGoal, function () {
                $scope.caseplanGoal = {};
                $scope.loadCaseplanGoals();
                $scope.caseplanView.showGoal = false;
                alertSuccess("Caseplan Goal saved!");
            });
        };

        $scope.selectMajorCategory = function () {
            staticInfoService.loadCasePlanGoalMinorCategories($scope.caseplanGoal.majorcategory_id.id, function (minorCategories) {
                $scope.minorCategories = minorCategories;
            }, function (message) {
                alertError(message);
            });
        };

        //caseplan Review
        $scope.caseplanReview = {};
        $scope.createCaseplanReview = function () {
            $scope.caseplanReview = {};
            $scope.caseplanView.showReview = true;
        };
        $scope.cancelReview = function () {
            $scope.caseplanReview = {};
            $scope.caseplanView.showReview = false;
        };
        $scope.selectCaseplanReview = function (review) {
            $scope.caseplanReview = angular.copy(review);
            $scope.caseplanView.showReview = true;
        };

        $scope.loadCaseplanReviews = function () {
            if ($scope.currentCaseplan) {
                clientService.loadCaseplanReviews($scope.currentCaseplan.id, function (reviews) {
                    $scope.caseplanReviews = reviews;
                }, function (errorMsg) {
                    alertError(errorMsg);
                });
            }
        };
        $scope.loadCaseplanReviews();

        $scope.selectReview = function(review){
            $scope.currentReview = review;
        };

        $scope.reviewItems = [];
        $scope.getReviewItems = function(){
            var goals = $scope.caseplanGoals;
            var reviewItems = [] ;
            angular.forEach(goals, function(goal){
                reviewItems.push({
                    goalID:goal.id,
                    majorCategory: goal.majorCategory,
                    minorCategory: goal.minorCategory,
                    severity:0
                });
            });
            $scope.reviewItems = reviewItems ;
       };

        $scope.saveReview = function () {
            var newReview = angular.copy($scope.caseplanReview);
            newReview.goals = [];

            angular.forEach($scope.reviewItems, function(r){
                newReview.goals.push({goalID:r.goalID, severity:r.severity});
            });

            newReview.caseplan_id = $scope.currentCaseplan.id;

            clientService.saveCaseplanReview(newReview, function () {
                $scope.caseplanReview = {};
                $scope.loadCaseplanReviews();
                $scope.caseplanView.showReview = false;
                alertSuccess("Caseplan Review saved!");
            }, function (errMessage) {
                alertError(errMessage);
            });
        };

        //choosing Casenote
        $scope.showCaseNoteChooser = function () {
            $scope.caseplanView.showCaseNoteChooser = true;
        };

        $scope.cancelCaseNote = function () {
            $scope.caseplanView.showCaseNoteChooser = false;
        };

        $scope.pickCaseNote = function (casenote) {
            clientService.addCaseplanCaseNote($scope.currentCaseplan.id, casenote.id, function () {
                $scope.loadCaseplanCaseNotes();
                $scope.loadCaseplanOpenCaseNotes();
            }, function (errMsg) {
                alertError(errMsg);
            });
        };

        $scope.dropCaseNote = function (casenote) {
            clientService.removeCaseplanCaseNote($scope.currentCaseplan.id, casenote.id, function () {
                $scope.loadCaseplanCaseNotes();
                $scope.loadCaseplanOpenCaseNotes();
            }, function (errMsg) {
                alertError(errMsg);
            });
        };

        $scope.deleteCaseNote = function(casenote){
            if(window.confirm("Are you sure you want to delete case note ID: " + casenote.id)){
                clientService.deleteCaseNote(casenote, function(){
                   $scope.loadCaseNotes();
                    alertSuccess("Casenote has been deleted");
                }, function(errMsg){
                    alertError(errMsg);
                });
            }
        };

        //Attachments
        $scope.selectFolder = function (folder) {
            $scope.currentFolder = folder;
            $scope.loadAttachments();
        };

        $scope.onAttachmentUpload = function () {
            alertSuccess("File uploaded!");
        };

        $scope.onAttachmentUpload = function (response) {
            $scope.loadAttachments();
            if (response.success) {
                alertSuccess("File Uploaded");
            } else {
                alertError("File upload failed.");
            }
        };

        $scope.attachments = [];
        $scope.loadAttachments = function () {
            attachmentService.loadClientAttachments({
                folderID: $scope.currentFolder.id,
                clientID: $scope.client.id
            }, function (attachments) {
                $scope.attachments = attachments;
            }, function (errMessage) {
                alertError(errMessage);
            });
        };

        $scope.deleteAttachment = function (attachment) {
            if (confirm("Are you sure you want to delete this attachment")) {
                attachmentService.deleteAttachment({attachmentName: attachment, clientID:$scope.client.id}, function () {
                    $scope.loadAttachments();
                    alertSuccess("Attachment deleted!");
                }, function (errMsg) {
                    alertError(errMsg);
                });
            }
            ;
        };

    }];


var CepCtrl = ["$scope",
    "staticInfoService",
    "cepService",
    "clientService",
    "attachmentService",
    function ($scope, staticInfoService, cepService, clientService, attachmentService) {

        $scope.cepOrganizations = [];
        $scope.loadStaticInfo = function () {
            if($scope.currentCard){
                $scope.orgID = $scope.currentCard.cep_organization_id.id;
            }
            staticInfoService.loadRecords("CepOrganization", function (records) {
                $scope.cepOrganizations = records;
                $scope.setCardOrganization();
                if($scope.currentCard){
                    $scope.currentCard.cep_organization_id = $scope.orgID ;
                }
                $scope.setCardDependencies();
                $scope.loadCepCards();
            });

            $scope.cepStatuses = ['NEW', 'IN PROGRESS', 'COMPLETE'];
            $scope.cardStatuses = ['ALL', 'NEW', 'IN PROGRESS', 'COMPLETE'];

            $scope.resourceTypes = ['BOOKLET', 'MAGAZINE', 'DVD', 'WHATEVER'];

            $scope.serviceTypes = ['EDUCATION', 'TRAINING', 'INFORMATION'];

        };
        $scope.loadStaticInfo();

        $scope.cepView = {};

        $scope.currentCard = {};

        $scope.cepCards = [];
        $scope.pageSize = 10;
        $scope.loadParams = {limit: $scope.pageSize};
        $scope.cardCount = 0;
        $scope.pages = [];
        $scope.getCardPages = function () {
            $scope.pages = [];
            if ($scope.cardCount > $scope.pageSize) {
                for (var i = 0; i < Math.ceil($scope.cardCount / $scope.pageSize); i++) {
                    $scope.pages.push(i + 1);
                }
            } else {
                $scope.pages.push(1);
            }
            ;
        };
        $scope.loadCepCards = function () {
            cepService.loadCards($scope.loadParams, function (result) {
                $scope.cepCards = result.records;
                $scope.cardCount = result.count;
                $scope.getCardPages();
                $scope.setCardOrganization();
            });
        };

        $scope.setCardOrganization = function (card) {
            angular.forEach($scope.cepCards, function (card) {
                var org =  findRecord($scope.cepOrganizations, card.cep_organization_id);
                card.cep_organization_id = findRecord($scope.cepOrganizations, card.cep_organization_id);
            });
        };

        $scope.selectCard = function (card) {
            $scope.currentCard = card;
            if ($scope.currentFolder) {
                $scope.loadAttachments();
            }
            $scope.cepView.tab = 'details';
            if (card.card_type == 'CLIENT') {
                if (card.client_id) {
                    clientService.loadClientByID({clientID: card.client_id}, function (client) {
                        $scope.selectedClient = client;
                    });
                } else {
                    $scope.selectedClient = null;
                }
            }
            $scope.loadNotes();
            $scope.loadInvoice();
            $scope.loadEvaluationParameters();
            $scope.loadSessionPlanByCepID();
        };

        $scope.currentPage = 1;
        $scope.goToPage = function (page) {
            $scope.loadParams.offset = (page - 1) * $scope.pageSize;
            $scope.loadParams.limit = $scope.pageSize;
            $scope.loadCepCards();
            $scope.currentPage = page;
        };

        $scope.canMovePrevious = function () {
            if ($scope.currentPage > 1) {
                return true;
            } else {
                return false;
            }
        };

        $scope.canMoveNext = function () {
            if ($scope.currentPage < $scope.pages.length) {
                return true;
            } else {
                return false;
            }
        };

        $scope.hideCardModal = function () {
            $scope.cepView.showEtiModal = false;
            $scope.cepView.showResourceModal = false;
            $scope.cepView.showClientModal = false;
        };

        $scope.createEtiCard = function () {
            $scope.currentCard = {card_type: "ETI", status: "NEW"};
            $scope.cepView.showEtiModal = true;
        };

        $scope.cancelCard = function () {
            $scope.currentCard = {};
            $scope.hideCardModal();
        };

        $scope.setCardDependencies = function () {
            console.log("CURRENT CARD ", $scope.currentCard);
            $scope.currentCard.cep_organization_id = findRecord($scope.cepOrganizations, $scope.currentCard.cep_organization_id);
        };

        $scope.saveCard = function () {
            var card = angular.copy($scope.currentCard);
            card.cep_organization_id = $scope.currentCard.cep_organization_id ? $scope.currentCard.cep_organization_id.id : null;
            cepService.saveCard(card, function (savedCard) {
                $scope.loadCepCards();
                $scope.hideCardModal();
                $scope.currentCard = savedCard;
                $scope.setCardDependencies($scope.currentCard);
                alertSuccess("Card saved!");
            });
        };

        $scope.createResourceCard = function () {
            $scope.currentCard = {card_type: "RESOURCE", status: "NEW"};
            $scope.cepView.showResourceModal = true;
        };

        $scope.createClientCard = function () {
            $scope.currentCard = {card_type: "CLIENT", status: "NEW"};
            $scope.cepView.showClientModal = true;
        };

        $scope.clients = [];
        $scope.showClientPicker = function () {
            $scope.cepView.showClientPicker = true;
            $scope.loadClients();
        };

        $scope.loadClients = function (filter) {
            clientService.loadClients({limit: 50, substring: filter}, function (result) {
                $scope.clients = result.records;
            });
        };

        $scope.selectClient = function (client) {
            $scope.selectedClient = client;
            $scope.currentCard.client_id = client.id;
            $scope.cepView.showClientPicker = false;
        };

        $scope.states = states;

        $scope.getCardPage = function () {
            if ($scope.currentCard) {
                switch ($scope.currentCard.card_type) {
                    case "ETI":
                        return '/views/eti-card.html';
                    case "CLIENT":
                        return '/views/client-card.html';
                    case "RESOURCE":
                        return '/views/resource-card.html';
                    default:
                        return '/views/no-card.html';
                }
            } else {
                return 'no-card.html';
            };
        };

        $scope.loadFolders = function () {
            staticInfoService.loadRecords("CepFolder", function (records) {
                $scope.folders = records;
            });
        };
        $scope.loadFolders();

        $scope.attachments = [];
        $scope.selectFolder = function (f) {
            $scope.currentFolder = f;
            $scope.loadAttachments();
        };

        $scope.loadAttachments = function () {
            attachmentService.loadCepAttachments({
                cepID: $scope.currentCard.id,
                cepFolderID: $scope.currentFolder.id
            }, function (attachments) {
                $scope.attachments = attachments;
            });
        };

        $scope.onAttachmentUpload = function () {
            $scope.loadAttachments();
        };

        $scope.notes = [];
        $scope.loadNotes = function () {
            if ($scope.currentCard.card_type == 'RESOURCE') {
                cepService.loadNotes($scope.currentCard.id, function (notes) {
                    $scope.notes = notes;
                });
            }
        };

        $scope.currentNote = {};
        $scope.saveNote = function () {
            var note = angular.copy($scope.currentNote);
            note.cepID = $scope.currentCard.id;
            cepService.saveNote(note, function () {
                $scope.loadNotes();
                $scope.currentNote = {};
                alertSuccess("Note saved!");
            });
        };

        $scope.currentInvoice = {};
        $scope.saveInvoice = function () {
            var invoice = angular.copy($scope.currentInvoice);
            invoice.cep_id = $scope.currentCard.id;
            cepService.saveInvoice(invoice, function () {
                $scope.loadInvoice();
                alertSuccess("Invoice saved!");
                $scope.cepView.showInvoice = false;
            });
        };
        $scope.loadInvoice = function () {
            cepService.loadInvoice($scope.currentCard.id, function (invoice) {
                if (invoice) {
                    $scope.currentInvoice = invoice;
                } else {
                    $scope.currentInvoice = {};
                }
            });
        };

        $scope.cancelInvoice = function () {
            $scope.loadInvoice();
            $scope.cepView.showInvoice = false;
        };

        $scope.copyDetailsFromCard = function () {
            $scope.currentInvoice.organization = $scope.currentCard.cep_organization_id ? $scope.currentCard.cep_organization_id.name : '';
            $scope.currentInvoice.contact_name = $scope.currentCard.contact_name;
            $scope.currentInvoice.contact_phone = $scope.currentCard.contact_name;
            $scope.currentInvoice.contact_phone = $scope.currentCard.contact_phone;
            $scope.currentInvoice.contact_email = $scope.currentCard.contact_phone;
            console.log($scope.currentInvoice);
        };


        $scope.participantEvaluations = [];
        $scope.facilitatorEvaluations = [];

        $scope.loadEvaluationParameters = function () {
            console.log("Loading evaluation parameters");
            staticInfoService.loadRecords("EvaluationParameter", function (records) {
                console.log("Evaluator Parameters::", records);
                $scope.participantEvaluations = [];
                $scope.facilitatorEvaluations = [];
                angular.forEach(records, function (record) {
                    if (record.use_for == 'FACILITATOR') {
                        $scope.facilitatorEvaluations.push({
                            cep_id: $scope.currentCard.id,
                            question: record.name,
                            feedback_type: record.feedback_type,
                            feedback_value: "",
                            use_for: "FACILITATOR"
                        });
                    }
                    ;

                    if (record.use_for == 'PARTICIPANT') {
                        $scope.participantEvaluations.push({
                            cep_id: $scope.currentCard.id,
                            question: record.name,
                            feedback_type: record.feedback_type,
                            feedback_value: "",
                            use_for: "PARTICIPANT"
                        });
                    }
                    ;
                });
                $scope.loadEvaluations();
            });
        };

        $scope.loadEvaluations = function () {
            cepService.loadEvaluations($scope.currentCard.id, function (evaluations) {
                if (evaluations) {
                    if (evaluations.length > 0) {
                        $scope.facilitatorEvaluations = [];
                        $scope.participantEvaluations = [];

                        angular.forEach(evaluations, function (record) {
                            if (record.use_for == 'FACILITATOR') {
                                $scope.facilitatorEvaluations.push({
                                    cep_id: $scope.currentCard.id,
                                    question: record.name,
                                    feedback_type: record.feedback_type,
                                    feedback_value: record.feedback_value,
                                    use_for: record.use_for
                                });
                            }
                            ;

                            if (record.use_for == 'PARTICIPANT') {
                                $scope.participantEvaluations.push({
                                    cep_id: $scope.currentCard.id,
                                    question: record.name,
                                    feedback_type: record.feedback_type,
                                    feedback_value: record.feedback_value,
                                    use_for: record.use_for
                                });
                            }
                            ;
                        });
                    }
                }
            });
        };

        $scope.saveEvaluations = function () {
            var evaluations = $scope.facilitatorEvaluations.concat($scope.participantEvaluations);
            cepService.saveEvaluations({
                cepID: $scope.currentCard.id,
                evaluations: evaluations
            }, function () {
                alertSuccess("Evaluation Saved!");
            });
        };

        $scope.setAgreeValue = function (evaluation, selection, value) {
            if (selection) {
                evaluation.feedback_value = value;
            } else {
                evaluation.feedback_value = null;
            };
        };

        $scope.currentSessionPlan = {};
        $scope.currentTopics = [];
        $scope.saveSessionPlan = function () {
            var sessionPlan = {
                id: $scope.currentSessionPlan.id,
                name: $scope.currentSessionPlan.name,
                cep_id: $scope.currentCard.id
            };
            cepService.saveSessionPlan(sessionPlan, function (id) {
                $scope.currentSessionPlan.id = id;
                alertSuccess("Session Saved.");
            });
        };

        $scope.loadSessionPlanByCepID = function () {
            cepService.loadSessionPlanByCepID($scope.currentCard.id, function (sessionPlan) {
                if (sessionPlan) {
                    $scope.currentSessionPlan = sessionPlan;
                    $scope.loadTopicBySessionPlanID(sessionPlan.id);
                } else {
                    $scope.currentSessionPlan = {};
                }
            });
        };

        $scope.loadTopicBySessionPlanID = function (sessionPlanID) {
            cepService.loadTopicBySessionPlanID(sessionPlanID, function (topics) {
                $scope.currentTopics = topics;
            });
        };

        $scope.getSessionPlanMinutes = function () {
            var total = 0;
            angular.forEach($scope.currentTopics, function (topic) {
                total = total + parseInt(topic.duration) || 0;
            });
            return total;
        };

        $scope.currentTopic = {};
        $scope.createTopic = function () {
            $scope.currentTopic = {};
            $scope.cepView.showTopicModal = true;
        };
        $scope.cancelTopic = function () {
            $scope.cepView.showTopicModal = false;
        };
        $scope.saveTopic = function () {
            var topic = angular.copy($scope.currentTopic);
            topic.cep_session_id = $scope.currentSessionPlan.id;
            cepService.saveTopic(topic, function () {
                $scope.loadTopicBySessionPlanID($scope.currentSessionPlan.id);
                $scope.cepView.showTopicModal = false;
                alertSuccess("Topic saved.");
            });
        };

        $scope.deleteTopic = function (topic) {
            if (window.confirm("Are you sure you want to delete '" + topic.topic + "'")) {
                cepService.deleteTopic(topic.id, function () {
                    $scope.loadTopicBySessionPlanID($scope.currentSessionPlan.id);
                    alertSuccess("Topic deleted.");
                });
            }
        };

        $scope.editTopic = function (topic) {
            $scope.currentTopic = angular.copy(topic);
            $scope.cepView.showTopicModal = true;
        };

        $scope.createOrganization = function(){
            $scope.currentOrganization = {};
            $scope.cepView.showOrganizationModal = true;
        };

        $scope.cancelOrganization = function(){
            $scope.currentOrganization = {};
            $scope.cepView.showOrganizationModal = false ;
        };

        $scope.saveOrganization = function(){
            staticInfoService.saveRecord({record: $scope.currentOrganization, recordType:"CepOrganization"}, function(){
                alertSuccess("Record saved!");
                $scope.currentOrganization = {};
                $scope.loadStaticInfo();
                $scope.cepView.showOrganizationModal = false ;
            });
        };

    }];

var ArticlesCtrl = ["$scope","$location", "$routeParams", "staticInfoService", "articleService",  function($scope, $location,$routeParams, staticInfoService, articleService){

    $scope.loadCategories = function(){
        staticInfoService.loadRecords("ArticleCategory", function(categories){
            var cats = [{id:0, name:'All Categories'}];
            angular.forEach(categories, function(c){
                cats.push(c);
            });
            $scope.categories = cats ;
        });
    };

    $scope.loadCategories();

    $scope.searchParams = {};

    $scope.articles = [];
    $scope.recordCount = 0;
    $scope.searchParams.limit = 10;
    $scope.searchParams.offset = ($routeParams.page - 1) * $scope.searchParams.limit || 0;
    $scope.loadArticles = function () {
        var params = {} ;
        params.query = $scope.searchParams.query;
        if ($scope.searchParams.category){
            params.categoryID = $scope.searchParams.category.id;
        }else{
            params.categoryID = 0 ;
        }
        params.offset = $scope.searchParams.offset ;
        params.limit = $scope.searchParams.limit ;
        articleService.search(params, function (result) {
            $scope.articles = result.records;
            $scope.recordCount = result.count;
        }, function (message) {
            alertError(message);
        });
    };
    $scope.loadArticles();

        // paging stuff
    $scope.currentPage = $routeParams.page || 1;
    $scope.getPages = function () {
        var pages = [];
        if ($scope.recordCount == 0) {
            return [];
        }
        for (var i = 1; i < Math.ceil($scope.recordCount / $scope.searchParams.limit) + 1; i++) {
            pages.push(i);
        }
        return pages;
    };

    $scope.movePrevious = function () {
        if ($scope.currentPage > 1) {
            $scope.goToPage($scope.currentPage - 1);
        }
    };

    $scope.moveNext = function () {
        if ($scope.currentPage < Math.ceil($scope.recordCount / $scope.searchParams.limit)) {
            $scope.goToPage($scope.currentPage + 1);
        };
    };

    $scope.doSearch = function(){
        $scope.loadArticles();
    };

    $scope.deleteArticle = function(article){
        if (window.confirm("Are you sure you want to delete article '"+article.title+"'")){
            articleService.deleteArticle(article.id, function(){
                alertSuccess("Article deleted.");
                $scope.loadArticles();
            });
        };
    };
}];

var ArticleCtrl = ["$scope","$routeParams","staticInfoService", "articleService","$location", function($scope, $routeParams, staticInfoService, articleService, $location){

    $scope.editorOptions = {
        language:'en'
    };

    $scope.loadCategories = function(){
        staticInfoService.loadRecords("ArticleCategory", function(records){
            $scope.categories = records ;
            if ($scope.article.category_id) {
                var cat = findRecord($scope.categories, $scope.article.category_id);
                $scope.article.category_id = cat ;
            };
        });
    };

    $scope.articleID = parseInt($routeParams.id)||0 ;
    $scope.content = "";
    $scope.loadArticle = function(){
        if ($scope.articleID > 0) {
            //load article by id
            articleService.loadByID($scope.articleID, function(article){
               if (article ){
                   $scope.article = article ;
                   $scope.content = article.content ;
                   $scope.loadCategories();
               }else {
                   $location.path("/article");
               }
            });
        } else {
           $scope.article = {status: "DRAFT"};
           $scope.loadCategories();
        }
    };

    $scope.loadArticle();

    $scope.statuses = ["DRAFT", "PUBLISHED"];

    $scope.saveArticle = function(){
        var a = angular.copy($scope.article);
        a.content = $scope.content ;
        a.category_id = $scope.article.category_id.id;
        articleService.saveArticle(a, function(id){
            $scope.article.id = id ;
            alertSuccess("Article saved.");
            $location.path("/article/"+id);
        });
    };

    $scope.cancelArticle = function(){
        $scope.loadArticle();
    };
}];

var UserCtrl = ["$scope","userService", function($scope, userService){

    $scope.users = [];
    $scope.searchParams = {};
    $scope.userView = {};
    $scope.doSearch = function(){
        userService.search($scope.searchParams.query, function(users){
            $scope.users = users ;
        });
    };
    $scope.doSearch();

    $scope.currentUser = {};
    $scope.createNewUser = function(){
        $scope.currentUser = {};
        $scope.userView.showNewUserModal = true ;
        console.log($scope.userView);
    };

    $scope.cancelNewUser = function(){
        $scope.currentUser = {};
        $scope.userView.showNewUserModal = false;
    };

    $scope.saveNewUser = function(){
        $scope.currentUser.role = $scope.currentUser.role?1:0;
        userService.save($scope.currentUser, function(id){
            alertSuccess("User saved.");
            $scope.currentUser.role = $scope.currentUser.role==1?true:false ;
            $scope.currentUser.active = $scope.currentUser.active==1?true:false;
            console.log(id);
            $scope.doSearch();
            $scope.userView.showNewUserModal = false ;
        });
    };

    $scope.editUser = function(user){
        $scope.currentUser = angular.copy(user) ;
        $scope.currentUser.role = user.role==1?true:false ;
        $scope.currentUser.active = user.active==1?true:false;
        $scope.userView.showNewUserModal = true ;
    };

    $scope.resetPassword = function(user){
        if (window.confirm("Are you sure you want to reset password for "+user.firstname+" "+user.lastname+"?")){
            userService.resetPassword(user.id, function(){
                alertSuccess("Password has been reset.");
            });
        };
    };
}];
