(ns ct2.data
  (:require [clojure.java.jdbc :as sql]
            [cheshire.core :as json]))

(def config  (json/parse-string (slurp (System/getenv "CASETRACK_CONFIG")) true))

 (def db-spec
   {:subprotocol "mysql"
    :subname (str "//" (:dbHost config) ":" (:dbPort config)  "/" (:dbName config))
    :user (:dbUser config)
    :password (:dbPass config)})

 (defn save-record
  [table-name record]
  (:generated_key (first (sql/insert! db-spec table-name record))))

(defn update-record
  [table-name record id]
  (sql/update! db-spec table-name record ["id = ?" id])
  id)

(defn update-record-by-column
  [table-name record column-name column-value]
  (first (sql/update! db-spec table-name record [(str column-name " = ?") column-value])))

(defn delete-record
  [table-name record-id]
  (sql/delete! db-spec table-name ["id = ?" record-id]))

(defn get-record-by-id
  [table-name record-id]
  (first (sql/query db-spec [(str "SELECT * FROM " table-name " WHERE id = ?") record-id])))

(defn get-all-records
  ([table-name limit offset]
    (sql/query db-spec [(str "SELECT * FROM " table-name " LIMIT ? OFFSET ?") limit offset]))
  ([table-name]
    (get-all-records table-name 100 0)))

(defn get-records-by-sql
  [sql & params]
  (sql/query db-spec (concat [sql] (or params []))))

(defn execute-query
  [query & params]
  (sql/execute! db-spec (concat [query] params)))
