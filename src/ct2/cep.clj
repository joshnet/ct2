(ns ct2.cep
  (:use ct2.data
        ct2.dateutils
        ct2.utils))

(defn save-card
  [card]
  (if-not (:id card)
    (pass (get-record-by-id
           "CepCard"
           (save-record "CepCard" (merge card {:created_on (get-current-timestamp)}))))
    (do
      (update-record "CepCard" (dissoc card :id :created_on) (:id card))
      (pass card))))

(def cep-card-select-sql
  "
SELECT c.* FROM CepCard c
LEFT JOIN CepOrganization o ON c.cep_organization_id = o.id
WHERE (status LIKE ? OR status IS NULL)
AND (LOWER(o.name) LIKE LOWER(?) OR LOWER(c.card_type) LIKE LOWER(?) OR c.card_type IS NULL OR LOWER(c.comments) LIKE LOWER(?) OR LOWER(c.client) LIKE LOWER(?)) AND c.card_type NOT LIKE ?
ORDER BY id DESC
LIMIT ? OFFSET ?")

(def count-sql
  "
SELECT count(c.id) as count FROM CepCard c
LEFT JOIN CepOrganization o ON c.cep_organization_id = o.id
WHERE (status LIKE ? OR status IS NULL)
AND (LOWER(o.name) LIKE LOWER(?) OR LOWER(c.card_type) LIKE LOWER(?) OR c.card_type IS NULL OR LOWER(c.comments) LIKE LOWER(?) OR LOWER(c.client) LIKE LOWER(?)) AND c.card_type NOT LIKE ?
")

(defn load-cards
  [params]
  (let [status (if (or (= (:status params) "ALL") (nil? (:status params))) "%" (:status params))
        substring (if (:substring params) (str "%" (:substring params)  "%") "%")
        cardtype (if (:showClient params) "shit" "CLIENT")
        limit (if (:limit params) (:limit params) 100)
        offset (if (:offset params) (:offset params) 0)
        records (get-records-by-sql cep-card-select-sql status substring substring substring substring cardtype limit offset)
        count (:count (first (get-records-by-sql count-sql status substring substring substring substring cardtype)))]
    (pass {:records records
           :count count})))


(defn save-note
  [note staff-id]
  (save-record "CepProjectNote"
              {:note (:note note)
               :cep_id (:cepID note)
               :created_by staff-id
               :created_on (get-current-timestamp)})
  (pass {}))

(defn cep-note-dto
  [note]
  {:id (:id note)
   :note (:note note)
   :createdBy (:username (get-record-by-id "Staff" (:created_by note)))
   :createdOn (timestamp-to-string (:created_on note))})

(defn load-notes
  [cep-id]
  (pass (map cep-note-dto (get-records-by-sql "SELECT * FROM CepProjectNote WHERE cep_id = ? ORDER BY id DESC" cep-id))))

(defn save-invoice
  [invoice]
  (if (:id invoice)
    (pass (update-record "CepInvoice" (dissoc invoice :id) (:id invoice)))
    (pass (save-record "CepInvoice" invoice))))

(defn load-invoice
  [cep-id]
  (pass (first  (get-records-by-sql "SELECT * FROM CepInvoice WHERE cep_id = ?" cep-id))))

(defn save-evaluations
  [cep-id evaluations]
  (execute-query "DELETE FROM CepEvaluation WHERE cep_id = ?" cep-id)
  (doseq [evaluation evaluations]
    (if (:id evaluation)
      (update-record "CepEvaluation" (dissoc evaluation :id :selection) (:id evaluation))
      (save-record "CepEvaluation" (dissoc evaluation :selection))))
  (pass {}))

(defn load-evaluations
  [cep-id]
  (pass (get-records-by-sql "SELECT * FROM CepEvaluation WHERE cep_id = ? " cep-id)))

(defn save-session-plan
  [session-plan]
  (if (:id session-plan)
    (pass (update-record "cep_session_plan" (dissoc session-plan :id) (:id session-plan)))
    (pass (save-record "cep_session_plan" session-plan))))

(defn load-session-plan
  [cep-id]
  (pass (first (get-records-by-sql "SELECT * FROM cep_session_plan WHERE cep_id = ? " cep-id))))

(defn load-topics
  [session-id]
  (pass (get-records-by-sql "SELECT * FROM cep_topic WHERE cep_session_id = ? " session-id)))

(defn delete-topic
  [topic-id]
  (delete-record "cep_topic" topic-id)
  (pass {}))

(defn save-topic
  [topic]
  (if (:id topic)
    (pass (update-record "cep_topic" (dissoc topic :id) (:id topic)))
    (pass (save-record "cep_topic" topic))))
