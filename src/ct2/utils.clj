(ns ct2.utils
  (:require [crypto.password.bcrypt :as encryptor]
            [selmer.parser :as parser]
            [postal.core :as mailer]
            [cheshire.core :as json]))

(def main-config  (json/parse-string (slurp (System/getenv "CASETRACK_CONFIG")) true))

(def email-config
  {:host (:emailHost main-config)
   :user (:emailUser main-config)
   :pass (:emailPass main-config)
   :ssl :yes
   :port (or (:emailPort main-config) 465)})

(defn pass
  ([data]
     {:success true :data data})
  ([]
     {:success true}))

(defn fail
  [message]
  {:success false :message message})

(defn is-email-valid?
  [email]
  (if (re-matches #"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" email) true false))

(defn encrypt-password
  [raw-password]
  (encryptor/encrypt raw-password))

(encrypt-password "pass")

(defn check-password
  [raw encrypted]
  (encryptor/check raw encrypted))

(defn send-email
  [to subject template-path data]
  (let [body (parser/render-file template-path data)
        send-result
        (mailer/send-message email-config
                             {:from (or (:emailSender main-config) "admin@crcc.com")
                              :to to
                              :subject subject
                              :body body})]
    (println send-result)))

(defn generate-password
  []
  (let [uuid (.toString (java.util.UUID/randomUUID))
        password (subs uuid 0 6)]
    password))
