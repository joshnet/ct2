(ns ct2.clientprinter
  (:use clj-pdf.core ct2.data ct2.dateutils)
  (:require [selmer.parser :as parser]))

(defn prepare-alert
  [alert]
  (merge alert
         {:status (if (:active alert) "Active" "Closed")
          :alerttype (:name (get-record-by-id "AlertType" (:alerttype_id alert)))
          :alertreason (:name (get-record-by-id "AlertReason" (:alertreason_id alert)))}))

(defn prepare-casenote
  [casenote]
  (merge casenote
         {:reason (:name (get-record-by-id "CaseNoteReason" (:reason_id casenote)))
          :contacttype (:name (get-record-by-id "CaseNoteContactType" (:contacttype_id casenote )))
          :outcome (:name (get-record-by-id "CaseNoteOutcome" (:outcome_id casenote)))
          :dateofservice (date-to-string (:dateofservice casenote))}))

(defn prepare-caseplan-goal
  [goal]
  (merge goal
         {:majorcategory (:name (get-record-by-id "CasePlanGoalCategory" (:majorcategory_id goal)))
          :minorcategory (:name (get-record-by-id "CasePlanGoalCategory" (:minorcategory_id goal)))
          :therapymethod (:name (get-record-by-id "TherapyMethod" (:therapymethod_id goal)))}))

(defn prepare-review-item
  [item]
  (let [goal (get-record-by-id "CasePlanGoal" (:goal_id item))]
    (merge item (prepare-caseplan-goal goal)
           {:severity (:currentseverity item)})))

(defn prepare-caseplan-review
  [review]
  (merge  review
          {:createdby (:username (get-record-by-id "Staff" (:createdby_id review)))
           :createdon (date-to-string (:createdon review))
           :items (map prepare-review-item (get-records-by-sql "SELECT * FROM ReviewItem WHERE review_id = ?" (:id review)))}))

(defn prepare-caseplan
  [caseplan]
  (merge caseplan
         {:closurecategory (:name (get-record-by-id "CasePlanClosureCategory" (:closurecategory_id caseplan)))
          :goals (map prepare-caseplan-goal (get-records-by-sql "SELECT * FROM CasePlanGoal WHERE caseplan_id = ?" (:id caseplan)))
          :reviews (map prepare-caseplan-review (get-records-by-sql "SELECT * FROM CasePlanReview WHERE caseplan_id = ?" (:id caseplan)))
          :casenotes (map prepare-casenote (get-records-by-sql "SELECT * FROM CaseNote WHERE caseplan_id = ? " (:id caseplan)))}))

(defn prepare-callout
  [callout]
  (merge callout
         {:famsacdate (date-to-string (:famsacdate callout))
          :dateofcontact (date-to-string (:dateofcontact callout))
          :sacat (if (:sacat callout) "Yes" "")
          :cancontactexternalservices (if (:cancontactexternalservices callout) "Yes" "")
          :chargeslaid (if (:chargeslaid callout) "Yes" "")
          :forensicexamconducted (if (:forensicexamconducted callout) "Yes" "")
          :formalstatement (if (:formalstatement callout) "Yes" "")
          :generaldutiespolice (if (:generaldutiespolice callout) "Yes" "")
          :incompletestatement (if (:incompletestatement callout) "Yes" "")
          :informalstatement (if (:informalstatement callout) "Yes" "")
          :ischildatrisk (if (:ischildatrisk callout) "Yes" "")
          :medicalexamconducted (if (:medicalexamconducted callout) "Yes" "")
          :meetandgreet (if (:meetandgreet callout) "Yes" "")
          :otherexam (if (:otherexam callout) "Yes" "")
          :otherpolice (if (:otherpolice callout) "Yes" "")
          :otherstatment (if (:otherstatment callout) "Yes" "")
          :pendingfurtherinvestigation (if (:pendingfurtherinvestigation callout) "Yes" "")
          :policenotified (if (:policenotified callout) "Yes" "")
          :reporttocare (if (:reporttocare callout) "Yes" "")}))


(defn parse-dates
  [client date-fields]
  (loop [fields date-fields updated-client client]
    (if (empty? fields)
      updated-client
      (recur
        (rest fields)
        (merge updated-client
          {(first fields) (date-to-string ((first fields) updated-client))})))))

(defn calculateAge
  [date]
  (if-not date
    0
    (age date)))

(defn- client-dto
  [client]
  (merge {:age (calculateAge (:dateofbirth client))} (parse-dates client
           [:famsacdate :adultsexualassultdate :assultbymultipleoffendersdate :assultedat1225date :childsexualassultdate :dateofbirth :dateofcontact :domesticviolencedate :drugalcoholfacilitatedsadate :firstcontactdate :firstdisclosuredate :incidentsviaelectronicmediadate :otherassultdate :recentincident :recentlyassulteddate :sexualharrassmentdate :specifyotherassultdate :stalkingdate :touchingoffencesdate :violencebyintimatepartnerdate :witnesssexualoffencedate])))

(defn prepare-client
  [client]
  (merge (client-dto client)
         {:dateofbirth (date-to-string (:dateofbirth client))
          :age (age (:dateofbirth client))
          :identifiedgender (:name (get-record-by-id "IdentifiedGender" (:identifiedgender_id client)))
          :counsellor (:username (get-record-by-id "Staff" (:counsellor_id client)))
          :firstcontactdate (date-to-string (:firstcontactdate client))
          :status (:name (get-record-by-id "ClientStatus" (:status_id client)))
          :homelessrisk (:name (get-record-by-id "HomelessRisk" (:homelessrisk_id client)))
          :ethnicity (:name (get-record-by-id "Ethnicity" (:ethnicity_id client)))
          :identifiedsexuality (:name (get-record-by-id "Sexuality" (:identifiedsexuality_id client)))

          :alerts (map prepare-alert (get-records-by-sql "SELECT * FROM Alert WHERE client_id = ? " (:id client)))
          :casenotes (map prepare-casenote (get-records-by-sql "SELECT * FROM CaseNote WHERE client_id = ?" (:id client)))
          :caseplans (map prepare-caseplan (get-records-by-sql "SELECT * FROM CasePlan WHERE client_id = ?" (:id client)))
          :callouts (map prepare-callout (get-records-by-sql  "SELECT * FROM Callout WHERE client_id = ?" (:id client)))
          :trauma (first (get-records-by-sql "SELECT * FROM Trauma WHERE client_id = ? " (:id client)))
          :sacat (or (:sacat client) false),

          :aboriginalcounsellorpreffered (if (:aboriginalcounsellorpreffered client) "Yes" "No")
          :adultsexualassult (if (:adultsexualassult client) "Yes" "")
          :alcoholuse (if (:alcoholuse client) "Yes" "")
          :assultbymultipleoffenders (if (:assultbymultipleoffenders client) "Yes" "")
          :assultedat1225 (if (:assultedat1225 client) "Yes" "")
          :cancontactexternalservices (if (:cancontactexternalservices client) "Yes" "")
          :canleavemessage (if (:canleavemessage client) "Yes" "")
          :cansendletters (if (:cansendletters client) "Yes" "")
          :cantext (if (:cantext client) "Yes" "")
          :chargeslaid (if (:chargeslaid client) "Yes" "")
          :childsexualassult (if (:childsexualassult client) "Yes" "")
          :counsellorallocated (if (:counsellorallocated client) "Yes" "")
          :crcc (if (:crcc client) "Yes" "")
          :difficulthearing (if (:difficulthearing client) "Yes" "")
          :difficultremembering (if (:difficultremembering client) "Yes" "")
          :difficultseeing (if (:difficultseeing client) "Yes" "")
          :difficultwalking (if (:difficultwalking client) "Yes" "")
          :disability (if (:disability client) "Yes" "")
          :domesticviolence (if (:domesticviolence client) "Yes" "")
          :drugalcoholfacilitatedsa (if (:drugalcoholfacilitatedsa client) "Yes" "")
          :druguse (if (:druguse client) "Yes" "")
          :eatingproblems (if (:eatingproblems client) "Yes" "")
          :forensicexamconducted (if (:forensicexamconducted client) "Yes" "")
          :formalstatement (if (:formalstatement client) "Yes" "")
          :generaldutiespolice (if (:generaldutiespolice client) "Yes" "")
          :incidentsviaelectronicmedia (if (:incidentsviaelectronicmedia client) "Yes" "")
          :incompletestatement (if (:incompletestatement client) "Yes" "")
          :informalstatement (if (:informalstatement client) "Yes" "")
          :interpreterneeded (if (:interpreterneeded client) "Yes" "")
          :ischildatrisk (if (:ischildatrisk client) "Yes" "")
          :medicalexamconducted (if (:medicalexamconducted client) "Yes" "")
          :meetandgreet (if (:meetandgreet client) "Yes" "")
          :mentalhealth (if (:mentalhealth client) "Yes" "")
          :nguru (if (:nguru client) "Yes" "")
          :otherexam (if (:otherexam client) "Yes" "")
          :otherhealth (if (:otherhealth client) "Yes" "")
          :otherpolice (if (:otherpolice client) "Yes" "")
          :otherstatement (if (:otherstatement client) "Yes" "")
          :pendingfurtherinvestigation (if (:pendingfurtherinvestigation client) "Yes" "")
          :policenotified (if (:plicenotified client) "Yes" "")
          :recentlyassulted (if (:recentlyassulted client) "Yes" "")
          :reporttocare (if (:reporttocare client) "Yes" "")
          :samssa (if (:samssa client) false)
          :selfharm (if (:selfharm client) "Yes" "")
          :sexualharrassment (if (:sexualharrassment client) "Yes" "")
          :stalking (if (:stalking client) "Yes" "")
          :suicidal (if (:suicidal client) "Yes" "")
          :touchingoffences (if (:touchingoffences client) "Yes" "")
          :violencebyintimatepartner (if (:violencebyintimatepartner client) "Yes" "")
          :witnesssexualoffence (if (:witnesssexualoffence client) "Yes" "")
          :wraparound (if (:wraparound client) "Yes" "")
          :counselloronly (if (:counselloronly client) "Yes" "")}))

(defn print-client-html
  [client-id]
  (let [client (prepare-client (get-record-by-id "Client" client-id))]
    (parser/render-file "templates/clientprint.html" client)))
