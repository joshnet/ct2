(ns ct2.attachment
  (:use ct2.data
        ct2.dateutils
        ct2.utils)
  (:require [clojure.java.io :as io]))

(def attachmentDir (:attachmentDir main-config))

(defn save-attachment
  [attachment]
  (println "SAVING ATTACHMENT " attachment)
  (println "ATTACHMENT DIR" attachmentDir)
  (let [purpose (:purpose attachment)
        folder_id (if (= purpose "CLIENT") (:folder_id attachment) (:cepfolder_id attachment))
        foldername (if (= purpose "CLIENT")
                     (:name (get-record-by-id "Folder" folder_id))
                     (:name (get-record-by-id "CepFolder" folder_id)))
        client_id (:client_id attachment)
        cep_id (:cep_id attachment)
        filename (:filename attachment)
        path (:path attachment)]
    (save-record "ClientFile"
                 (merge attachment {:created_on (get-current-timestamp)}))
    (io/make-parents (io/file (str attachmentDir "/" purpose "/" foldername "/" (if (= purpose "CLIENT") client_id cep_id) "/" filename)))
    (io/copy (io/file  path) (io/file (str attachmentDir "/" purpose "/" foldername "/" (if (= purpose "CLIENT") client_id cep_id) "/" filename))))
  (pass {:success true}))

(defn load-client-attachments2
  [folder-id client-id]
  (pass (get-records-by-sql "SELECT * FROM ClientFile WHERE folder_id = ? AND client_id = ?" folder-id client-id)))

(defn load-client-attachments
  [folder-id client-id]
  (let [foldername (:name (get-record-by-id "Folder" folder-id))
        folder (io/file (str attachmentDir "/CLIENT/" foldername "/" client-id))
        files (file-seq folder)
        filenames (map #(.getName %) (rest files))]
    (pass filenames)))

(defn load-cep-attachments2
  [folder-id cep-id]
  (pass (get-records-by-sql "SELECT * FROM ClientFile WHERE folder_id = ? AND cep_id = ?" folder-id cep-id)))

(defn load-cep-attachments
  [folder-id cep-id]
  (let [foldername (:name (get-record-by-id "CepFolder" folder-id))
        folder (io/file (str attachmentDir "/CEP/" foldername "/" cep-id))
        files (file-seq folder)
        filenames (map #(.getName %) (rest files))]
    (pass filenames)))

(defn delete-attachment
  [attachment-name client-id]
  (let [attachment (first (get-records-by-sql "SELECT * FROM ClientFile WHERE filename = ? AND client_id = ? " attachment-name client-id))
        client_id (:client_id attachment)
        filename (:filename attachment)
        cep_id (:cep_id attachment)
        purpose (:purpose attachment)
        folder_id (:folder_id attachment)
        foldername (if (= purpose "CLIENT")
                     (:name (get-record-by-id "Folder" folder_id))
                     (:name (get-record-by-id "CepFolder" folder_id)))]
    ;(println attachment)
    (io/delete-file (str attachmentDir "/" purpose "/" foldername "/" (if (= purpose "CLIENT") client_id cep_id) "/" filename))
    (pass (delete-record "ClientFile" (:id attachment)))))
