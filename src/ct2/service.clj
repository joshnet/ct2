(ns ct2.service
  (:use ct2.dateutils
        ct2.data
        ct2.static-info
        ct2.utils
        ct2.user)
  (:require [clojure.java.jdbc :as sql]
            [clostache.parser :as tpl]
            [ct2.user :as user]
            [ct2.client :as client]
            [ct2.cep :as cep]
            [ct2.article :as article]
            [ct2.attachment :as attachment]
            [clojure.java.io :as io])
  (:import [java.sql Timestamp]))

(def upload-dir "/usr/local/uploads/")

(defn record-create
  [params]
  (static-record-create (:recordType params) (:record params) (:id params)))

(defn record-delete
  [params]
  (static-record-delete (:recordType params) (:id params)))

(defn record-loadall
  [params]
  (static-record-load (:recordType params) (:activeOnly params) (:limit params) (:offset params)))

(defn record-loadcaseplangoalmajorcategories
  [params]
  (load-caseplan-goal-major-categories))

(defn record-loadcaseplangoalminorcategories
  [params]
  (load-caseplan-goal-minor-categories (:majorCategoryID params)))

(defn user-login
  [params]
  (user/login (:username params) (:password params)))

(defn user-logout
  [params]
  (user/logout (:sessionKey params)))

(defn user-changepassword
  [params]
  (user/change-password
    (:oldPassword params)
    (:newPassword params)
    (:newPasswordConfirmation params)
    (:sessionKey params)))

(defn user-loadall
  [params]
  (user/load-all-users))

(defn user-search
  [params]
  (search-user (:query params)))

(defn user-save
  [params]
  (save-user (dissoc params :sessionKey)))

(defn user-resetpassword
  [params]
  (reset-password (:userID params)))

(defn user-resetpasswordbyemail
  [params]
  (reset-password-by-email (:email params)))

                                        ;CLIENT API

(defn client-load
  [params]
  (client/get-clients params))

(defn client-advancedsearch
  [params]
  (client/advanced-search params))

(defn client-save
  [params]
  (client/save params))

(defn client-load-by-id
  [params]
  (client/load-by-id (:clientID params)))

(defn client-loadalerts
  [params]
  (client/load-alerts (:clientID params) (:substring params) (:activeOnly params)))

(defn client-savealert
  [params]
  (client/save-alert (dissoc params :sessionKey) (get-user-by-session-key (:sessionKey params))))

(defn client-closealert
  [params]
  (client/close-alert (dissoc params :sessionKey) (get-user-by-session-key (:sessionKey params))))

(defn client-reopenalert
  [params]
  (client/reopen-alert (dissoc params :sessionKey) (get-user-by-session-key (:sessionKey params))))

(defn client-acknowledgealert
  [params]
  (let [staff-id (:staff_id (get-session-by-session-key (:sessionKey params)))]
    (client/acknowledge-alert (:alertID params) staff-id)))

(defn client-loadmyalerts
  [params]
  (let [staff-id (:staff_id (get-session-by-session-key (:sessionKey params)))]
    (client/load-my-alerts staff-id (:clientID params))))

(defn client-loadallmyalerts
  [params]
  (let [staff-id (:staff_id (get-session-by-session-key (:sessionKey params)))]
    (client/load-all-my-alerts staff-id)))

(defn client-loadcasenotes
  [params]
  (client/load-case-notes (:clientID params) (:substring params)))

(defn client-savecasenote
  [params]
  (client/save-case-note params))

(defn client-deletecasenote
  [params]
  (let [staff-id (:staff_id (get-session-by-session-key (:sessionKey params)))
        staff (get-record-by-id "Staff" staff-id)]
    (if-not staff
      (fail "You are not authorized to delete case notes.")
      (client/delete-casenote params))))

(defn client-savecallout
  [params]
  (let [staff-id (:staff_id (get-session-by-session-key (:sessionKey params)))
        staff (get-record-by-id "Staff" staff-id)]
    (if-not staff
      (fail "You are not authorized to delete case notes.")))
  (client/save-callout (dissoc params :sessionKey)))

(defn client-loadcallout
  [params]
  (client/load-callout (:calloutID params)))

(defn client-loadcallouts
  [params]
  (client/load-callouts (:clientID params)))

(defn client-loadtrauma
  [params]
  (client/load-trauma (:clientID params)))

(defn client-savetrauma
  [params]
  (client/save-trauma (dissoc params :sessionKey)))

(defn client-loadcaseplans
  [params]
  (client/load-caseplans (:clientID params)))

(defn client-savecaseplan
  [params]
  (client/save-caseplan
   (-> params
    (dissoc  :sessionKey :counsellor)
    (merge {:createdby_id (:staff_id (get-session-by-session-key (:sessionKey params)))}))))

(defn client-savecaseplangoal
  [params]
  (client/save-caseplan-goal (dissoc params :sessionKey :majorCategory :minorCategory :therapyMethod :createdOn :datecreated)))

(defn client-loadcaseplangoals
  [params]
  (client/load-caseplan-goals (:caseplanID params)))

(defn client-savecaseplanreview
  [params]
  (client/save-caseplan-review
   (-> params
       (dissoc :sessionKey :createdOn :createdBy :createdon)
       (merge {:createdby_id (:staff_id (get-session-by-session-key (:sessionKey params)))}))))

(defn client-loadcaseplanreviews
  [params]
  (client/load-caseplan-reviews (:caseplanID params)))

(defn client-removecaseplancasenote
  [params]
  (client/remove-caseplan-casenote (:caseplanID params) (:casenoteID params)))

(defn client-addcaseplancasenote
  [params]
  (client/add-caseplan-casenote (:caseplanID params) (:casenoteID params)))

(defn client-loadcaseplancasenotes
  [params]
  (client/load-caseplan-casenotes (:caseplanID params)))

(defn client-loadopencasenotes
  [params]
  (client/load-open-casenotes (:clientID params) (:caseplanID params)))

(defn client-delete
  [params]
  (client/delete (:clientID params) (:sessionKey params)))

(defn client-merge
  [params]
  (client/merge-clients (:targetID params) (:sourceID params) (:sessionKey params)))

(defn upload-attachment
  [params session-key]
  (attachment/save-attachment
   (merge  params {:uploaded_by (:staff_id (get-session-by-session-key session-key))})))

(defn attachment-loadforclient
  [params]
  (attachment/load-client-attachments (:folderID params) (:clientID params)))

(defn attachment-loadforcep
  [params]
  (attachment/load-cep-attachments (:cepFolderID params) (:cepID params)))

(defn attachment-delete
  [params]
  (attachment/delete-attachment (:attachmentName params) (:clientID params)))

(defn file-download
  [fileID]
  (let [file (get-record-by-id "ClientFile" fileID)]
    (if-not file
      {:body "File not found"
       :status 404}
      {:body (io/input-stream (:path file))
       :status 200
       :headers {"Content-Type" (:content_type file)}})))

                                        ;CEP API

(defn cep-savecard
  [params]
  (cep/save-card (dissoc params :sessionKey)))

(defn cep-loadcards
  [params]
  (cep/load-cards params))

(defn cep-savenote
  [params]
  (cep/save-note (dissoc params :sessionKey) (:staff_id (get-session-by-session-key (:sessionKey params)))))

(defn cep-loadnotes
  [params]
  (cep/load-notes (:cepID params)))

(defn cep-saveinvoice
  [params]
  (cep/save-invoice (dissoc params :sessionKey)))

(defn cep-loadinvoice
  [params]
  (cep/load-invoice (:cepID params)))

(defn cep-saveevaluations
  [params]
  (cep/save-evaluations (:cepID params) (:evaluations params)))

(defn cep-loadevaluations
  [params]
  (cep/load-evaluations (:cepID params)))

(defn cep-savesessionplan
  [params]
  (cep/save-session-plan (dissoc params :sessionKey)))

(defn cep-loadsessionplan
  [params]
  (cep/load-session-plan (:cepID params)))

(defn cep-loadtopics
  [params]
  (cep/load-topics (:sessionPlanID params)))

(defn cep-savetopic
  [params]
  (cep/save-topic (dissoc params :sessionKey)))

(defn cep-deletetopic
  [params]
  (cep/delete-topic (:topicID params)))

(defn article-save
  [params]
  (article/save (dissoc params :sessionKey) (:staff_id (get-session-by-session-key (:sessionKey params)))))

(defn article-delete
  [params]
  (article/delete (:articleID params)))

(defn article-search
  [params]
  (article/search (:cartegoryID params) (:query params) (:offset params) (:limit params)))

(defn article-loadbyid
  [params]
  (article/load-by-id (:articleID params)))
