(ns ct2.user
  (:use ct2.data ct2.dateutils ct2.utils))

(defn get-user-by-username
  [username]
  (first (get-records-by-sql "SELECT * FROM Staff WHERE username = ?"  username)))

(defn get-user-by-email
  [email]
  (first (get-records-by-sql "SELECT * FROM Staff WHERE email = ?"  email)))

(defn kill-sessions
  [user]
  (update-record-by-column "UserSession"
                 {:active false :dateLoggedOut (get-current-timestamp)}
                 "staff_id" (:id user)))

(defn create-user-session
  [user]
  (kill-sessions user)
  (let [sessionKey (.toString (java.util.UUID/randomUUID))]
    (save-record "UserSession"
                 {:active true
                  :dateLoggedIn  (get-current-timestamp)
                  :sessionKey sessionKey
                  :staff_id (:id user)})
    sessionKey))

(defn get-session-by-session-key
  [session-key]
  (first (get-records-by-sql "SELECT * FROM UserSession WHERE sessionKey = ? " session-key)))

(defn check-session-key
  [session-key]
  (let [session (get-session-by-session-key session-key)]
    (:active session)))

(defn fail-login
  []
  (fail "Invalid login credentials"))

(defn login
  [username password]
  (let [user (get-user-by-username username)]
    (cond
     (not user) (fail-login)
     (not (:active user)) (fail "Your account has been deactivated.")
     (not (check-password password (:password user))) (fail-login)
     :else (pass {:username (:username user) :sessionKey (create-user-session user) :role (:role user)}))))


(defn direct-login
  [email]
  (let [user (get-user-by-email email)]
    (cond
     (not user) (fail "Your email address is not recognized. Please contact the system administrator.")
     (not (:active user)) (fail "Your account has been deactivated.")
     :else (pass {:username (:username user) :sessionKey (create-user-session user) :role (:role user)}))))

(defn logout
  [session-key]
  (update-record-by-column
    "UserSession"
    {:active false :dateLoggedOut (get-current-timestamp)}
    "sessionKey"
    session-key)
  (pass {}))

(defn get-user-by-session-key
  [session-key]
  (get-record-by-id "Staff" (:staff_id (get-session-by-session-key session-key))))

(defn update-password
  [sessionKey newPassword]
  (let [user-id (:staff_id (get-session-by-session-key sessionKey))]
    (update-record "Staff" {:password (encrypt-password newPassword)} user-id)))

(defn change-password
  [old-password new-password new-confirmation session-key]
  (cond
   (not old-password) (fail "You must provide the old password.")
   (not new-password) (fail "You must provide new password.")
   (not= new-password new-confirmation) (fail "Passwords do not match")
   (not (check-password  old-password  (:password (get-user-by-session-key session-key)))) (fail "Incorrect old password")
   :else
   (pass (update-password session-key new-password))))

(defn- user-dto
  [user]
  (merge user {:fullname (str (:firstname user) " " (:lastname user))}))

(defn load-all-users
  []
  (pass (map user-dto (get-all-records "Staff"))))

(defn search-user
  [query]
  (let [query-str (str "%" query "%")]
    (pass
     (map #(dissoc % :password) (get-records-by-sql
            "SELECT * FROM Staff WHERE LOWER(firstName) LIKE LOWER(?) OR LOWER(lastName) LIKE LOWER(?) OR LOWER(username) LIKE LOWER(?) OR LOWER(email) LIKE LOWER(?) ORDER BY username ASC"
            query-str query-str query-str query-str)))))

(defn is-username-unique?
  [new-user]
  (let [user (get-user-by-username (:username new-user))]
    (if user
      (if (= (:id user) (:id new-user))
        true false)
      true)))

(defn is-email-unique?
  [new-user]
  (let [users (get-records-by-sql "SELECT * FROM Staff WHERE email = ?" (:email new-user))
        user (filter #(= (:id %) (:id new-user)) users)]
    (if (zero? (count users))
      true
      (if user
        true false))))

(defn save-user
  [new-user]
  (cond
   (nil? (:username new-user)) (fail "You must provide username")
   (nil? (:firstname new-user)) (fail "You must provide firstname")
   (nil? (:lastname new-user)) (fail "You must provide lastname")
   (nil? (:email new-user)) (fail "You must provide a valid email address")
   (not (is-username-unique? new-user)) (fail "Username is already in use")
   (not (is-email-unique? new-user)) (fail "Email is already in use")
   (not (is-email-valid? (:email new-user))) (fail "Invalid email address")
   :else
   (let [user {:id (:id new-user)
               :username (:username new-user)
               :firstName (:firstname new-user)
               :lastName (:lastname new-user)
               :email (:email new-user)
               :role (or (:role new-user) 0)
               :active (or (:active new-user) false)}]
     (if (:id user)
       (do
         (update-record "Staff" (dissoc user :id) (:id user))
         (pass (:id new-user)))
       (let [passwd (generate-password)
             new-password (encrypt-password passwd)]
         (send-email (:email user) "Password Reset" "email/passwordreset.txt" (merge user {:password passwd}))
         (pass (save-record "Staff" (merge user {:password new-password}))))))))

(defn reset-password
  [user-id]
  (let [password (generate-password)
        new-password (encrypt-password password)
        user (get-record-by-id "Staff" user-id)]
    (update-record "Staff" {:password new-password} user-id)
    (send-email (:email user) "Password Reset" "email/passwordreset.txt" (merge user {:password password}))
    (pass)))

(defn reset-password-by-email
  [email]
  (let [user (first (get-records-by-sql "SELECT * FROM Staff WHERE email = ?" email))]
    (if user (reset-password (:id user)))
    (pass)))
