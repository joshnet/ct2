(ns ct2.client
  (:use ct2.data
        ct2.dateutils
        ct2.utils
        ct2.user)
  (:require [ct2.user :as user]))

(def page-size 25)

(defn parse-dates
  [client date-fields]
  (loop [fields date-fields updated-client client]
    (if (empty? fields)
      updated-client
      (recur
        (rest fields)
        (merge updated-client
          {(first fields) (date-to-string ((first fields) updated-client))})))))

(defn calculateAge
  [date]
  (if-not date
    0
    (age date)))

(defn- client-dto
  [client]
  (merge {:age (calculateAge (:dateofbirth client))} (parse-dates client
           [:famsacdate :adultsexualassultdate :assultbymultipleoffendersdate :assultedat1225date :childsexualassultdate :dateofbirth :dateofcontact :domesticviolencedate :drugalcoholfacilitatedsadate :firstcontactdate :firstdisclosuredate :incidentsviaelectronicmediadate :otherassultdate :recentincident :recentlyassulteddate :sexualharrassmentdate :specifyotherassultdate :stalkingdate :touchingoffencesdate :violencebyintimatepartnerdate :witnesssexualoffencedate])))

(defn- alert-dto
  [alert]
  (let [client (get-record-by-id "Client" (:client_id alert))]
    (merge alert
      {:alertType (:name (get-record-by-id "AlertType" (:alerttype_id alert)))
       :client (str (:firstname client) " " (:lastname client))})))


(defn- case-note-dto
  [casenote]
  (merge casenote {:dateofservice (date-to-string (:dateofservice casenote))
                   :contactType (:name (get-record-by-id "CaseNoteContactType" (:contacttype_id casenote)))
                   :author (:username (get-record-by-id "Staff" (:author_id casenote)))}))

(defn- caseplan-dto
  [caseplan]
  (merge caseplan
    {:counsellor (:username (get-record-by-id "Staff" (:createdby_id caseplan)))}))

(defn- caseplan-goal-dto
  [caseplan-goal]
  (merge
    caseplan-goal
    {:therapyMethod (:name (get-record-by-id "TherapyMethod" (:therapymethod_id caseplan-goal)))
     :majorCategory (:name (get-record-by-id "CasePlanGoalCategory" (:majorcategory_id caseplan-goal)))
     :minorCategory (:name (get-record-by-id "CasePlanGoalCategory" (:minorcategory_id caseplan-goal)))
     :createdOn (date-to-string (:datecreated caseplan-goal))}))

(defn- review-goal-dto
  [review-goal]
  (let [goal (get-record-by-id "CasePlanGoal" (:goal_id review-goal))
        goal-dto (caseplan-goal-dto goal)]
    (merge goal-dto {:severity (:currentseverity review-goal)})))

(defn- get-review-goals
  [review-id]
  (let [review-goals (get-records-by-sql "SELECT * FROM ReviewItem WHERE review_id = ?" review-id)]
    (map review-goal-dto review-goals)))

(defn- caseplan-review-dto
  [caseplan-review]
  (merge
    caseplan-review
    {:createdBy (:username (get-record-by-id "Staff" (:createdby_id caseplan-review)))
     :createdOn (date-to-string (:createdon caseplan-review))
     :goals (get-review-goals (:id caseplan-review))}))


(defn param-to-string-query-part
  [param]
  (str (:prop param)
       (cond
        (= (:op param) "contains") (str " LIKE " "'%" (:value param) "%'")
        (= (:op param) "startsWith") (str " LIKE '" (:value param) "%'")
        (= (:op param) "endsWith") (str " LIKE " "'%" (:value param) "'")
        :else (str " LIKE '" (:value param) "'"))))

(defn param-to-number-query-part
  [param]
  (str (:prop param)
       (cond
        (= (:op param) "=") (str " = " (:value param))
        (= (:op param) ">") (str " > " (:value param))
        (= (:op param) "<") (str " < " (:value param))
        (= (:op param) ">=") (str " >= " (:value param))
        (= (:op param) "<=") (str " >= " (:value param))
        :else (str " = " (:value param)))))

(defn param-to-boolean-query-part
  [param]
  (str (:prop param)
       (cond
        (= (:op param) "is") (str " = " (if (:value param) 1 0))
        :else (str " = " (if (:value param) 1 0)))))

(defn generate-in-clause
  [prop op list]
  (let [parts (map #(str prop  op %) list)]
    (str "(" (clojure.string/join " or " parts) ")")))

(defn param-to-list-query-part
  [param]
    (cond
   (= (:op param) "is") (generate-in-clause (:prop param) "=" (:value param))
   :else (generate-in-clause (:prop param) "=" (:value param))))

(defn generate-query-part
  [param]
  (cond
   (= (:prop param) "firstname") (param-to-string-query-part param)
   (= (:prop param) "lastname") (param-to-string-query-part param)
   (= (:prop param) "othername") (param-to-string-query-part param)
   (= (:prop param) "filenumber") (param-to-number-query-part param)
   (= (:prop param) "dateofbirth") (param-to-number-query-part param)
   (= (:prop param) "SAMSSA") (param-to-boolean-query-part param)
   (= (:prop param) "Nguru") (param-to-boolean-query-part param)
   (= (:prop param) "CRCC") (param-to-boolean-query-part param)
   (= (:prop param) "RoyalCommission") (param-to-boolean-query-part param)
   (= (:prop param) "counsellor") (param-to-list-query-part (merge param {:prop "counsellor_id"}))
   (= (:prop param) "ID") (param-to-number-query-part param)))

(defn generate-query
  [params user]
  (let [parts (map generate-query-part params)]
    (if (empty? parts)
      (if (zero? (:role user)) (str "WHERE (counsellor_id = " (:id user) " OR counselloronly = 0) "))
      (str " WHERE " (clojure.string/join " and " parts) (if (zero? (:role user)) (str " and counsellor_id = " (:id user)))))))

(defn advanced-search
  [queryParams]
  (println "WHERE CLAUSE " (generate-query (:params queryParams) (get-user-by-session-key (:sessionKey queryParams))))
  (let [user (get-user-by-session-key (:sessionKey queryParams))
        whereClause   (generate-query (:params queryParams) user)
        limit (or (:limit queryParams) page-size)
        offset (or (:offset queryParams) 0)
        recordCount (:count (first (get-records-by-sql (str "SELECT COUNT(*) as count FROM Client " whereClause) )))
        records (get-records-by-sql (str "SELECT * FROM Client " whereClause " LIMIT ? OFFSET ? ") limit offset)]
    (pass {:records (map client-dto records)
           :count recordCount})))

(defn generate-client-load-whereclause
  [params]
  (let [user (get-user-by-session-key (:sessionKey params))
        role (:role user)]
    (str
     "(id LIKE ? OR firstName LIKE ? OR otherName LIKE ? OR lastName LIKE ? OR fileNumber LIKE ? OR dateOfBirth LIKE ? ) "
     (if (:mineOnly params) " AND counsellor_id = ? ")
     (if (:samssaOnly params) " AND samssa = ?")
     (if (zero? role) " AND (counsellor_id = ? OR counselloronly = 0)"))))

(defn generate-client-load-params
  [params]
  (let [substr (if (:substring params) (str "%" (:substring params) "%") "%")
        staffID (:staff_id (user/get-session-by-session-key (:sessionKey params)))
        user (get-record-by-id "Staff" staffID)
        role (:role user)]
    (filter #(not (nil? %))
      (vector substr substr substr substr substr substr
        (if (:mineOnly params) staffID nil)
        (if (:samssaOnly params) true nil)
        (if (zero? role) staffID nil)))))

(defn load-clients
  [params]
  (let [query (str "SELECT * FROM Client WHERE "
                   (generate-client-load-whereclause params)
                (str " LIMIT ? OFFSET ? "))
        query-params (generate-client-load-params params)]
    (apply get-records-by-sql (concat [query] query-params [(or (:limit params) page-size) (or (:offset params) 0)]))))

(defn count-clients
  [params]
  (let [query (str "SELECT COUNT(*) as count FROM Client WHERE " (generate-client-load-whereclause params))
        params (generate-client-load-params params)]
    (:count (first (apply get-records-by-sql (concat [query] params))))))

(defn get-clients
  [params]
  (pass {:records (map client-dto (load-clients params))
         :count (count-clients params)}))

(defn create-client
  [client]
  (merge (dissoc client :sessionKey :age)
    {:dateofbirth (string-to-timestamp (:dateofbirth client))
     :dateofcontact (string-to-timestamp (:dateofcontact client))
     :firstcontactdate (string-to-timestamp (:firstcontactdate client))
     :sacat (or (:sacat client) false),
     :aboriginalcounsellorpreffered (or (:aboriginalcounsellorpreffered client) false)
     :adultsexualassult (or (:adultsexualassult client) false)
     :alcoholuse (or (:alcoholuse client) false)
     :assultbymultipleoffenders (or (:assultbymultipleoffenders client) false)
     :assultedat1225 (or (:assultedat1225 client) false)
     :cancontactexternalservices (or (:cancontactexternalservices client) false)
     :canleavemessage (or (:canleavemessage client) false)
     :cansendletters (or (:cansendletters client) false)
     :cantext (or (:cantext client) false)
     :chargeslaid (or (:chargeslaid client) false)
     :childsexualassult (or (:childsexualassult client) false)
     :counsellorallocated (or (:counsellorallocated client) false)
     :crcc (or (:crcc client) false)
     :difficulthearing (or (:difficulthearing client) false)
     :difficultremembering (or (:difficultremembering client) false)
     :difficultseeing (or (:difficultseeing client) false)
     :difficultwalking (or (:difficultwalking client) false)
     :disability (or (:disability client) false)
     :domesticviolence (or (:domesticviolence client) false)
     :drugalcoholfacilitatedsa (or (:drugalcoholfacilitatedsa client) false)
     :druguse (or (:druguse client) false)
     :eatingproblems (or (:eatingproblems client) false)
     :forensicexamconducted (or (:forensicexamconducted client) false)
     :formalstatement (or (:formalstatement client) false)
     :generaldutiespolice (or (:generaldutiespolice client) false)
     :incidentsviaelectronicmedia (or (:incidentsviaelectronicmedia client) false)
     :incompletestatement (or (:incompletestatement client) false)
     :informalstatement (or (:informalstatement client) false)
     :interpreterneeded (or (:interpreterneeded client) false)
     :ischildatrisk (or (:ischildatrisk client) false)
     :medicalexamconducted (or (:medicalexamconducted client) false)
     :meetandgreet (or (:meetandgreet client) false)
     :mentalhealth (or (:mentalhealth client) false)
     :nguru (or (:nguru client) false)
     :otherexam (or (:otherexam client) false)
     :otherhealth (or (:otherhealth client) false)
     :otherpolice (or (:otherpolice client) false)
     :otherstatement (or (:otherstatement client) false)
     :pendingfurtherinvestigation (or (:pendingfurtherinvestigation client) false)
     :policenotified (or (:plicenotified client) false)
     :recentlyassulted (or (:recentlyassulted client) false)
     :reporttocare (or (:reporttocare client) false)
     :samssa (or (:samssa client) false)
     :selfharm (or (:selfharm client) false)
     :sexualharrassment (or (:sexualharrassment client) false)
     :stalking (or (:stalking client) false)
     :suicidal (or (:suicidal client) false)
     :touchingoffences (or (:touchingoffences client) false)
     :violencebyintimatepartner (or (:violencebyintimatepartner client) false)
     :witnesssexualoffence (or (:witnesssexualoffence client) false)
     :wraparound (or (:wraparound client) false)
     :counselloronly (or (:counselloronly client) false)
     :postcode 2600
     :recentincident (string-to-date (:recentincident client))
     :recentlyassulteddate (string-to-date (:recentlyassulteddate client))
     :assultedat1225date (string-to-date (:assultedat1225date client))
     :drugalcoholfacilitatedsadate (string-to-date (:drugalcoholfacilitatedsadate client))
     :incidentsviaelectronicmediadate (string-to-date (:incidentsviaelectronicmediadate client))
     :touchingoffencesdate (string-to-date (:touchingoffencesdate client))
     :childsexualassultdate (string-to-date (:childsexualassultdate client))
     :adultsexualassultdate (string-to-date (:adultsexualassultdate client))
     :assultbymultipleoffendersdate (string-to-date (:assultbymultipleoffendersdate client))
     :violencebyintimatepartnerdate (string-to-date (:violencebyintimatepartnerdate client))
     :sexualharrassmentdate (string-to-date (:sexualharrassmentdate client))
     :stalkingdate (string-to-date (:stalkingdate client))
     :witnesssexualoffencedate (string-to-date (:witnesssexualoffencedate client))
     :domesticviolencedate (string-to-date (:domesticviolencedate client))
     :otherassult (or (:otherassult client) false)
     :otherassultdate (string-to-date (:otherassultdate client))
     :firstdisclosuredate (string-to-date (:firstdisclosuredate client))
     :famsacdate (string-to-date (:famsacdate client))
     :specifyotherassultdate (string-to-date (:specifyotherassultdate client))}))

(defn save
  [client]
  (cond
    (not (or (:firstName client) (:firstname client))) (fail "You must provide firstName")
    (not (or (:lastName client) (:lastname client))) (fail "You must provide lastName")
    (is-future-date (:dateofbirth client)) (fail "Date of birth cannot be in the future")
    (is-future-date (:firstcontactdate client)) (fail "First contact date cannot be in the future")
    :else (if (:id client)
            (pass (update-record "Client" (dissoc (create-client client) :id) (:id client)))
            (pass (save-record "Client" (create-client client))))))

(defn load-by-id
  [client-id]
  (if (not client-id)
    (fail "You must provide clientID")
    (pass (client-dto (get-record-by-id "Client" client-id)))))

(defn load-alerts
  [client-id substring active-only]
  (let [substr (if substring (str "%" substring "%") "%")
        active (or active-only false)]
    (pass (map alert-dto (get-records-by-sql
                           "SELECT * FROM Alert WHERE (active = ? OR active = ?)  AND
 issue LIKE ? AND client_id = ? ORDER BY active  DESC, id DESC"
                           true active substr client-id)))))


(defn create-staff-alerts
  [alert-id]
  ;create alerts only if client is accessible to the staff
  (let [staffs (get-all-records "Staff")
        alert (get-record-by-id "Alert" alert-id)
        client (get-record-by-id "Client" (:client_id alert))]
    (doseq [staff staffs]
      (if (or (not (:counselloronly client)) (= (:counsellor_id (:id staff))))
        (save-record "staff_alert"
                     {:staff_id (:id staff)
                      :alert_id alert-id
                      :is_acknowledged false})))))

(defn acknowledge-alert
  [alert-id staff-id]
  (execute-query "UPDATE staff_alert SET is_acknowledged = true WHERE alert_id = ? AND staff_id = ? ", alert-id, staff-id)
  (pass))

(defn get-all-pending-alerts
  [staff-id]
  (map alert-dto
    (get-records-by-sql
      "SELECT * FROM Alert WHERE id IN (SELECT alert_id FROM staff_alert WHERE staff_id = ? AND is_acknowledged = false)" staff-id)))

(defn load-my-alerts
  [staff-id client-id]
  (let [pending-alerts (get-all-pending-alerts staff-id)]
    (pass (filter #(= (:client_id %) client-id) pending-alerts))))

(defn load-all-my-alerts
  [staff-id]
  (pass
    (get-all-pending-alerts staff-id)))

(defn save-alert
  [alert user]
  (cond
    (nil? (:issue alert)) (fail "You must provide the alert description")
    :else (if (:id alert)
            (do
              (update-record "Alert" (dissoc (merge alert {:permanent (or (:permanent alert) false)}) :id :client :closedon :closedby_id :openedon :openedby_id) (:id alert))
              (if (:active alert) (create-staff-alerts (:id alert)))
              (pass (:id alert)))
            (let [alert-id (save-record "Alert" (dissoc (merge alert {:permanent (or (:permanent alert) false)
                                                                       :openedon (get-current-timestamp)
                                                                       :openedby_id (:id user)}) :client))]
              (if (:active alert) (create-staff-alerts alert-id))
              (pass alert-id)))))

(defn close-alert
  [alert user]
  (save-alert (dissoc (merge alert {:closedon (get-current-timestamp)
                                     :closedby_id (:id user)
                                     :active false}) :client) user))

(defn reopen-alert
  [alert user]
  (save-alert (dissoc (merge alert {:active true
                                     :closedby_id nil
                                     :closedon nil}) :client) user))

(defn load-case-notes
  [client-id substr]
  (let [substring (if substr (str "%" substr "%") "%")]
    (pass (map case-note-dto (get-records-by-sql
                               "SELECT *, STR_TO_DATE(timeofservice, '%h:%i %p') as timex FROM CaseNote WHERE client_id = ? AND (note LIKE ? OR id LIKE ?) ORDER BY dateofservice DESC, timex DESC"
                               client-id substring substring)))))


(defn prepare-casenote
  [casenote]
  (merge (dissoc casenote :contactType :datecreated :sessionKey :author :timex)
         {:dateofservice (string-to-timestamp (:dateofservice casenote))
          :author_id (:id (get-user-by-session-key (:sessionKey casenote)))}))

(defn is-author-or-admin
  [casenote]
  (let [performer_id (:id (get-user-by-session-key (:sessionKey casenote)))
        author_id (:author_id casenote)
        performer (get-record-by-id "Staff" performer_id)]
    (println "PEFORMER ID" performer_id)
    (println "AUTHOR ID" author_id)
    (if (:id casenote)
      (or (= performer_id author_id) (= (:role performer) 1))
      true)))

(defn save-case-note
  [casenote]
  (cond
   (nil? (:dateofservice casenote)) (fail "You must provide date of service")
   (nil? (:contacttype_id casenote)) (fail "You must select service provided")
   (nil? (:duration casenote)) (fail "You must provide duration")
   (not (> (:duration casenote) 0)) (fail "Duration must be greater than 0")
   (nil? (:outcome_id casenote)) (fail "You must provide outcome")
   (nil? (:reason_id casenote)) (fail "You must provide a reason")
   (nil? (:note casenote)) (fail "You must provide a note")
   (nil? (:timeofservice casenote)) (fail "You must provide time of service")
   (not (is-author-or-admin casenote)) (fail "You must be the author or the system admin to edit the casenote.")
   :else
    (if (:id casenote)
      (pass (update-record "CaseNote" (dissoc (prepare-casenote (dissoc casenote :id)) :author_id) (:id casenote)))
      (pass (save-record "CaseNote" (merge (prepare-casenote casenote)
                                           {:datecreated (get-current-timestamp)}))))))

(defn delete-casenote
  [casenote]
  (execute-query "DELETE FROM CaseNote WHERE id = ?" (:id casenote))
  (pass))

(defn prepare-callout
  [callout]
  (merge callout
    {:sacat (or (:sacat callout) false)
     :cancontactexternalservices (or (:cancontactexternalservices callout) false)
     :chargeslaid (or (:chargeslaid callout) false)
     :forensicexamconducted (or (:forensicexamconducted callout) false)
     :formalstatement (or (:formalstatement callout) false)
     :generaldutiespolice (or (:generaldutiespolice callout) false)
     :incompletestatement (or (:incompletestatement callout) false)
     :informalstatement (or (:informalstatement callout) false)
     :ischildatrisk (or (:ischildatrisk callout) false)
     :medicalexamconducted (or (:medicalexamconducted callout) false)
     :meetandgreet (or (:meetandgreet callout) false)
     :otherexam (or (:otherexam callout) false)
     :otherpolice (or (:otherpolice callout) false)
     :otherstatment (or (:otherstatment callout) false)
     :pendingfurtherinvestigation (or (:pendingfurtherinvestigation callout) false)
     :policenotified (or (:policenotified callout) false)
     :reporttocare (or (:reporttocare callout) false)
     :dateofcontact (string-to-date (:dateofcontact callout))
     :famsacdate (string-to-date (:famsacdate callout))}))

(defn save-callout
  [callout]
  (if (:id callout)
    (pass (update-record "Callout" (dissoc (prepare-callout callout) :id) (:id callout)))
    (pass (save-record "Callout" (prepare-callout callout)))))

(defn callout-dto
  [callout]
  (merge callout
         {:dateofcontact (date-to-string (:dateofcontact callout))}))

(defn load-callout
  [calloutID]
  (pass (callout-dto (get-record-by-id "Callout" calloutID))))

(defn load-callouts
  [clientID]
  (pass (map callout-dto (get-records-by-sql "SELECT * FROM Callout WHERE client_id = ? " clientID))))

(defn load-trauma
  [clientID]
  (pass (first (get-records-by-sql "SELECT * FROM Trauma WHERE client_id = ? " clientID))))

(defn save-trauma
  [trauma]
  (if (:id trauma)
    (pass (update-record "Trauma" (dissoc trauma :id) (:id trauma)))
    (pass (save-record "Trauma" trauma))))

(defn load-caseplans
  [clientID]
  (pass (map caseplan-dto (get-records-by-sql "SELECT * FROM CasePlan WHERE client_id = ?" clientID))))


(defn save-caseplan
  [caseplan]
  (if (:id caseplan)
    (pass (update-record "CasePlan" (dissoc caseplan :id) (:id caseplan)))
    (pass (caseplan-dto (get-record-by-id "CasePlan" (save-record "CasePlan" caseplan))))))

(defn save-caseplan-goal
  [goal]
  (if (:id goal)
    (pass (update-record "CasePlanGoal" (dissoc goal :id) (:id goal)))
    (pass (save-record "CasePlanGoal"
            (merge goal {:datecreated (get-current-timestamp)})))))

(defn load-caseplan-goals
  [caseplan-id]
  (pass (map caseplan-goal-dto (get-records-by-sql "SELECT * FROM CasePlanGoal WHERE caseplan_id = ?" caseplan-id))))

(defn save-caseplan-review
  [review]
  (if (:id review)
    (pass (update-record "CasePlanReview" (dissoc review :id) (:id review)))
    (let [review-id (save-record "CasePlanReview"
                                 (dissoc (merge review {:createdOn (get-current-timestamp)}) :goals :severity))]
      (doseq [goal (:goals review)]
        (save-record "ReviewItem" {:goal_id (:goalID goal) :review_id review-id :currentseverity (:severity goal)}))
      (pass))))

(defn load-caseplan-reviews
  [caseplan-id]
  (pass (map caseplan-review-dto (get-records-by-sql "SELECT * FROM CasePlanReview WHERE caseplan_id = ?" caseplan-id))))


(defn load-open-casenotes
  [client-id caseplan-id]
  (pass (get-records-by-sql "SELECT * FROM CaseNote WHERE client_id = ? AND (caseplan_id IS NULL OR caseplan_id = ?)" client-id caseplan-id)))

(defn load-caseplan-casenotes
  [caseplan-id]
  (pass (get-records-by-sql "SELECT * FROM CaseNote WHERE caseplan_id = ? " caseplan-id)))

(defn add-caseplan-casenote
  [caseplan-id, casenote-id]
  (pass (update-record "CaseNote" {:caseplan_id caseplan-id} casenote-id)))

(defn remove-caseplan-casenote
  [caseplan-id casenote-id]
  (pass (update-record "CaseNote" {:caseplan_id nil} casenote-id)))

(defn delete-client-dependencies
  [client-id]
  (execute-query "DELETE FROM Alert WHERE client_id = ?" client-id)
  (execute-query "DELETE FROM Callout WHERE client_id = ?" client-id)
  (execute-query "DELETE FROM CaseNote WHERE client_id = ?" client-id)
  (execute-query "DELETE FROM ReviewItem WHERE review_id IN (SELECT id FROM CasePlanReview WHERE caseplan_id IN (SELECT id FROM CasePlan WHERE client_id = ?))" client-id)
  (execute-query "DELETE FROM CasePlanGoal WHERE caseplan_id IN (SELECT id FROM CasePlan WHERE client_id = ?)" client-id)
  (execute-query "DELETE FROM CasePlanReview WHERE caseplan_id IN (SELECT id FROM CasePlan WHERE client_id = ?)" client-id)
  (execute-query "DELETE FROM CaseNote WHERE caseplan_id IN (SELECT id FROM CasePlan WHERE client_id = ?)" client-id)
  (execute-query "DELETE FROM CasePlan WHERE client_id = ?" client-id)
  (execute-query "DELETE FROM CepCard WHERE client_id = ?" client-id)
  (execute-query "DELETE FROM ClientFile WHERE client_id = ?" client-id)
  (execute-query "DELETE FROM Trauma WHERE client_id = ?" client-id))

(defn delete
  [client-id session-key]
  (let [session (get-session-by-session-key session-key)
        user (get-record-by-id "Staff" (:staff_id session))]
    (if (not= (:role user) 1)
      (fail "You must be an administrator to delete a record")
      (do
        (delete-client-dependencies client-id)
        (delete-record "Client" client-id)
        (pass)))))

(defn copy-client
  [target source]
  (loop [keys (keys target) result target]
    (if (empty? keys)
      result
      (recur (rest keys)
        (if-not (result (first keys))
          (merge result {(first keys) (source (first keys))})
          result)))))

(defn merge-clients
  [target-id source-id session-key]
  (let [session (get-session-by-session-key session-key)
        user (get-record-by-id "Staff" (:staff_id session))]
    (if (not= (:role user) 1)
      (fail "You must be an administrator to delete a record")
      (do
        (execute-query "UPDATE Alert SET client_id = ? WHERE client_id = ? " target-id source-id)
        (execute-query "UPDATE Callout SET client_id = ? WHERE client_id = ? " target-id source-id)
        (execute-query "UPDATE CaseNote SET client_id = ? WHERE client_id = ? " target-id source-id)
        (execute-query "UPDATE CasePlan SET client_id = ? WHERE client_id = ? " target-id source-id)
        (execute-query "UPDATE CepCard SET client_id = ? WHERE client_id = ? " target-id source-id)
        (execute-query "UPDATE ClientFile SET client_id = ? WHERE client_id = ? " target-id source-id)
        (execute-query "UPDATE Trauma SET client_id = ? WHERE client_id = ? " target-id source-id)

        (let [source-client (get-record-by-id "Client" source-id)
              target-client (get-record-by-id "Client" target-id)]
          (update-record "Client" (dissoc
                                    (copy-client target-client source-client) :id) target-id))
        (delete-client-dependencies source-id)
        (execute-query "DELETE FROM Client WHERE id = ? " source-id)
        (pass)))))

;(defn create-100-clients
;  []
;  (let [session-key (user-login {:usernme "Josh" :password "pass"})
;        firstNames ["John","Peter", "Paul", "James", "Simon", "David", "Daniel", "Joseph", "Jacob" ]
;        lastNames ["Smith", "May", "Clackson", "Jackson", "Bush", "Green", "Elton", "Bell"]
;        otherNames ["Gray", "Graham", "Kowalsky", "Watson", "Martin", "Lawrence", "Simpson"]]
;    (doseq [x (range 101)]
;      (let [client {:firstName (get firstNames (rand-int (count firstNames)))
;                    :lastName (get lastNames (rand-int (count lastNames)))
;                    :otherName (get otherNames (rand-int (count otherNames)))
;                    :fileNumber (rand-int 10000)
;                    :dateOfBirth (str (inc (rand-int 27)) "/"
;                                      (inc (rand-int 11)) "/"
;                                      (+ 1900 (rand-int 100)))}]
;        (client-save client)))))

;(create-100-clients)
