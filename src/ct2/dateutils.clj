(ns ct2.dateutils
  (:import [java.sql Timestamp]
           [java.text SimpleDateFormat]))

(def DEFAULT_DATE_FORMAT "dd/MM/yyyy")
(def DEFAULT_TIMESTAMP_FORMAT "dd/MM/yyyy @ hh:mm a")

(defn string-to-date
  [datestr]
  (try
    (let [sdf (SimpleDateFormat. DEFAULT_DATE_FORMAT)]
      (.parse sdf datestr))
    (catch Exception e
      nil)))

(defn string-to-timestamp
  [datestr]
  (if-not datestr
    nil
    (let [date (string-to-date datestr)]
      (if date
        (Timestamp. (.getTime date))
        nil))))

(defn get-current-timestamp
  []
  (Timestamp. (java.lang.System/currentTimeMillis)))

(defn date-to-string
  [d]
  (cond
    (nil? d) ""
    :else
    (let [sdf (SimpleDateFormat. DEFAULT_DATE_FORMAT)]
      (.format sdf d))))

(defn timestamp-to-string
  [t]
  (cond
   (nil? t) ""
   :else
   (let [sdf (SimpleDateFormat. DEFAULT_TIMESTAMP_FORMAT)]
     (.format sdf t))))

(defn age
  [dob]
  (try
    (let [sdf (SimpleDateFormat. "yyyy")
          yob (.format sdf dob)
          current-year (.format sdf (get-current-timestamp))]
      (- (Integer. current-year) (Integer. yob)))
    (catch Exception e
      0)))

(defn is-future-date
  [date-str]
  (cond
   (nil? date-str) false
   (empty? date-str) false
   (nil? (string-to-timestamp date-str)) false
   :else
   (> (.getTime (string-to-timestamp date-str)) (.getTime (get-current-timestamp)))))
