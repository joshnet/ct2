(ns ct2.article
  (:use ct2.data
         ct2.dateutils
         ct2.utils)
  (:require ct2.user))

(defn save
  [article user-id]
  (if-not (:id article)
    (pass (save-record "article"
                       (merge article
                              {:created_on (get-current-timestamp)
                               :updated_on (get-current-timestamp)
                               :created_by user-id
                               :updated_by user-id})))
    (do (update-record "article" (-> article
                                       (dissoc :id :created_on :updated_on)
                                       (merge {:updated_on (get-current-timestamp)
                                               :updated_by user-id})) (:id article))
        (pass (:id article)))))

(defn delete
  [article-id]
  (delete-record "article" article-id)
  (pass))

(defn article-dto
  [article]
  {:id (:id article)
   :category (:name (get-record-by-id "ArticleCategory" (:category_id article)))
   :title (:title article)})

(defn search-without-category
  [query offset limit]
  (let [select-sql "SELECT * FROM article WHERE LOWER(title) LIKE LOWER(?) OR LOWER(content) LIKE LOWER(?)"
        count-sql "SELECT COUNT(*) AS count FROM article WHERE LOWER(title) LIKE LOWER(?) OR LOWER(content) LIKE LOWER(?)"
        records (map article-dto  (get-records-by-sql (str select-sql " LIMIT ? OFFSET ?") query query limit offset))
        count (:count (first (get-records-by-sql count-sql query query)))]
    {:records records :count count}))

(defn search-with-category
  [category-id query offset limit]
  (let [select-sql "SELECT * FROM article WHERE category_id =? AND ( LOWER(title) LIKE LOWER(?) OR LOWER(content) LIKE LOWER(?))"
        count-sql "SELECT COUNT(*) AS count FROM article WHERE category_id =? AND ( LOWER(title) LIKE LOWER(?) OR LOWER(content) LIKE LOWER(?))"
        records (map article-dto (get-records-by-sql (str select-sql " LIMIT ? OFFSET ?") category-id query query limit offset))
        count (:count (first (get-records-by-sql count-sql category-id query query)))]
    {:records records :count count}))

(defn search
  [category-id query offset limit]
  (let [search-query (str "%" query "%")]
    (if (zero? (or category-id 0))
      (pass (search-without-category search-query offset limit) )
      (pass (search-with-category category-id search-query offset limit)))))


(defn load-by-id
  [article-id]
  (pass (get-record-by-id "article" article-id)))
