(ns ct2.static-info
  (:use ct2.data
        ct2.utils))

(defn validate-record
  [record-type]
  {:success true :message nil})

(defn static-record-create
  [record-type record  id]
  (cond
    (not record-type) (fail "You must provide recordtype")
    :else
    (if id
      (pass (update-record record-type record id ))
      (pass (save-record record-type record)))))

(defn static-record-delete
  [record-type record-id]
  (cond
   (not record-type) (fail "You must provide recordType")
   (not record-id) (fail "You must provide record id")
   :else (pass (delete-record record-type record-id))))

(defn static-record-load
  [record-type active-only the-limit the-offset]
  (let [limit (or the-limit 100)
        offset (or the-offset 0)]
    (cond
      (not record-type) (fail "You must provide recordType")
      :else
      (pass (get-records-by-sql (str "SELECT * FROM " record-type " WHERE active = true OR active = ? LIMIT ? OFFSET ? ") active-only limit offset)))))

(defn load-caseplan-goal-major-categories
  []
  (pass (get-records-by-sql "SELECT * FROM CasePlanGoalCategory WHERE majorCategory_id IS NULL")))

(defn load-caseplan-goal-minor-categories
  [major-category-id]
  (cond
   (not major-category-id) (fail "You must provide major category")
   :else
   (pass
     (get-records-by-sql
       "SELECT * FROM CasePlanGoalCategory WHERE majorCategory_id = ?" major-category-id))))
