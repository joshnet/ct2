(ns ct2.handler
  (:use compojure.core ct2.service ct2.user ct2.googleauth ct2.data ct2.clientprinter clj-pdf.core ct2.attachment)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [cheshire.core :as json]
            [clojure.java.io :as io]
            [ring.util.response :as resp]
            [selmer.parser :as parser]))

(defn get-params
  [request]
  (json/parse-string (slurp (:body request)) true))

(defn get-service-name
  [uri]
  (println "Getting service::" + uri)
  (let [uri-parts (clojure.string/split uri #"\/")]
    (str (get uri-parts 2) "-" (get uri-parts 3))))

(defn sanitize-filename
  [filename]
  (clojure.string/replace filename  #"\s" "_"))

(defroutes app-routes
  (GET "/" request
       (let [from-google (:redir (:params request))]
         (if from-google
           (let [email (google-user-email (google-access-token request))]
             (let [login-result (direct-login email)]
               (if (:success login-result)
                 {:status 200
                  :body (io/input-stream (io/resource "public/index.html"))
                  :headers {"Content-Type" "text/html"}
                  :cookies {"authsession" {:value (json/generate-string {:sessionKey (:sessionKey (:data login-result))
                                                                         :username (:username (:data login-result))
                                                                         :role (:role (:data login-result))})}}}
                 (:message login-result))))
           {:body (io/input-stream (io/resource "public/index.html"))
            :status 200
            :headers {"Content-Type" "text/html"}})))

  (GET "/googleauth" request
       (resp/redirect (:uri auth-req)))

  (GET "/oauth2/redirect" request
       (resp/redirect "/"))

  (GET "/client/:id/:sessionKey" [id sessionKey]
       (let [user (get-user-by-session-key sessionKey)
             client (get-record-by-id "Client"  id)]
         (if (zero? (:role user))
           "You are not allowed to print client detail."
           {:status 200
            :body (print-client-html id)
            :headers {"Content-Type" "text/html"}})))

  ;API HANDLER
  (POST "/api/*" request
        (try
          (let [service-func (resolve (symbol "ct2.service" (get-service-name (:uri request))))
                params (get-params request)]
            (println params)
            (if (or (= (:uri request) "/api/user/login")
                    (= (:uri request) "/api/user/logout")
                    (= (:uri request) "/api/user/resetpasswordbyemail"))
              (json/generate-string (service-func params))
              (if (check-session-key (:sessionKey params))
                (json/generate-string (service-func params))
                (json/generate-string {:success false :message "Invalid session key"}))))
          (catch Exception e
            (.printStackTrace e)
            (json/generate-string {:success false :message (.getMessage e)}))))

   (POST "/file/upload" request
         (try
           (let [params (:multipart-params request)
                 uploaded-file (params "uploadedFile")
                 folder-id (params "folderID")
                 cepfolder_id (params "cepFolderID")
                 purpose (params "purpose")
                 client-id (params "clientID")
                 cep-id (params "cepID")
                 size (:size uploaded-file)
                 content-type (:content-type uploaded-file)
                 filename (:filename uploaded-file)
                 path (str upload-dir (System/currentTimeMillis) "_" (sanitize-filename filename))
                 cookies (:cookies request)
                 userInfo (json/parse-string (:value (cookies "userInfo")))
                 sessionKey (userInfo "sessionKey")]
             (if-not (check-session-key sessionKey) (throw (Exception. "Invalid session key")))
             (io/copy (:tempfile uploaded-file) (io/file path))
             {:body (json/generate-string (upload-attachment
                                           {:folder_id folder-id
                                            :cepfolder_id cepfolder_id
                                            :size size
                                            :content_type content-type
                                            :filename filename
                                            :path path
                                            :purpose purpose
                                            :client_id client-id
                                            :cep_id cep-id} sessionKey))
              :status 200
              :headers {"Content-Type" "application/json"}})
           (catch Exception e
             {:body (json/generate-string {:success false :message (.getMessage e)})
              :status 200
              :headers {"Content-Type" "application/json"}})))

   (GET "/file/download/:purpose/:ownerID/:foldername/:filename" request
        (try
          (let [fileID (:fileID (:route-params request))
                purpose (:purpose (:route-params request))
                ownerID (:ownerID (:route-params request))
                foldername (:foldername (:route-params request))
                filename (:filename (:route-params request))
                cookies (:cookies request)
                userInfo (json/parse-string (:value (cookies "userInfo")))
                sessionKey (userInfo "sessionKey")]
            (if-not (check-session-key sessionKey)
              {:body "You are not authorized to view this file"
               :status 401}
              {:body (io/input-stream (io/file (str attachmentDir "/" purpose "/" foldername "/" ownerID "/" filename)))
               :status 200}))
          (catch Exception e
            {:body "You are not authorized to view this file"
             :status 401})))

   (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (handler/site app-routes))
