var alertSuccess = function(message) {
	$().toastmessage("showSuccessToast", message);
};

var alertError = function(message) {
	$().toastmessage("showErrorToast", message);
};

var findRecord = function(collection, id) {
	var record = null
	for (var i = 0; i < collection.length; i++) {
		if (collection[i].id == id) {
			record = collection[i];
		}
	}
	return record;

};

var ct = angular.module("ct", [ "ngCookies", "ngRoute" ]);

ct.directive("onEnter", [ "$parse", function($parse) {
	return {
		restrict : "A",
		link : function(scope, el, attr) {
			el.on("keyup", function(event) {
				if (event.keyCode == 13) {
					scope[attr.onEnter]();
				}
			});
		}
	};
} ]);

ct.directive("mainMenu", [ "$cookieStore", "$location", "userService",
		function($cookieStore, $location, userService) {
			return {
				restrict : "E",
				templateUrl : "/views/mainmenu.html",
				link : function(scope, el, attr) {
					scope.logout = function() {
						userService.logout(function() {
							$cookieStore.remove("userInfo");
							$location.path("/");
						}, function(message) {
							alertError(message);
						});

					};
					scope.changePassword = function() {
						$("#changePasswordDialog").modal({
							show : true,
							backdrop : 'static',
							keyboard : 'false'
						});
					};
					scope.isUserLoggedIn = function() {
						var userInfo = $cookieStore.get("userInfo");
						if (userInfo) {
							return true;
						}
						return false;
					};
					scope.getUserInfo = function() {
						return $cookieStore.get("userInfo");
					};
				}
			};
		} ]);

ct.directive("changePasswordDialog", [ "userService", function(userService) {
	return {
		restrict : "E",
		templateUrl : "/views/changepassworddialog.html",
		scope : {},
		link : function(scope, el, attr) {
			scope.passwords = {};
			scope.closeDialog = function() {
				scope.passwords = {};
				$("#changePasswordDialog").modal('hide');
			};

			scope.changePassword = function() {
				userService.changePassword(scope.passwords, function() {
					scope.passwords = {};
					$("#changePasswordDialog").modal('hide');
					alertSuccess("Password changed.");
				}, function(message) {
					alertError(message);
				});
			};
		}
	};
} ]);

ct.directive("datemask", [ function() {
	return {
		restrict : "A",
		link : function(scope, el, attr) {
			$(el).inputmask('d/m/y');
		}
	};
} ]);

ct.directive("datepicker", [ function() {
	return {
		restrict : 'A',
		link : function(scope, el, attr) {
			$(el).attr("data-date-format", "DD/MM/YYYY");
			$(el).datetimepicker({
				pickTime : false
			});
		}
	};
} ]);

ct.directive("timepicker", [ function() {
	return {
		restrict : "A",
		link : function(scope, el, attr) {
			$(el).datetimepicker({
				pickDate : false
			});
		}
	};
} ]);

ct.config([ "$routeProvider", "$locationProvider",
		function($routeProvider, $locationProvider) {
			// $locationProvider.hashPrefix("!");
			$routeProvider.when("/", {
				templateUrl : "views/login.html",
				controller : LoginCtrl,
				name : "login"
			}).when("/staticinfo", {
				templateUrl : "views/staticinfo.html",
				controller : StaticInfoCtrl,
				name : "staticinfo"
			}).when("/clients", {
				templateUrl : "views/client-list.html",
				controller : ClientsCtrl,
				name : "clients"
			}).when("/clients/:clientID", {
				templateUrl : "views/client.html",
				controller : ClientCtrl,
				name : "clients"
			}).otherwise({
				redirectTo : "/clients"
			});
		} ]);

ct.run([ "$rootScope", "$cookieStore", "$location",
		function($rootScope, $cookieStore, $location) {
			$rootScope.session = {};
			$rootScope.$on("$routeChangeStart", function(event, next, current) {
				var userInfo = $cookieStore.get("userInfo");
				if (!userInfo) {
					if (next.name != "login") {
						$location.path("/");
					}
				}
				$rootScope.session.currentPage = next.name;
			});
		} ]);

ct.factory("ajax", [ "$http", "$cookieStore", "$location",
		function($http, $cookieStore, $location) {
			return {
				doPost : function(url, data, successCb, errorCb) {
					var userInfo = $cookieStore.get("userInfo");
					if (userInfo) {
						data.sessionKey = userInfo.sessionKey;
					}
					;

					$http.post(url, data).success(function(response) {
						if (response.success) {
							successCb(response.data);
						} else {
							console.log("ERROR:" + response.message);
							if (response.message === "Invalid session key") {
								$cookieStore.remove("userInfo");
								$location.path("/");
								return;
							}
							errorCb(response.message);
						}
					}).error(function(httpError) {
						console.log(httpError);
					});
				}
			};
		} ]);

ct.factory("userService", [
		"$http",
		"$location",
		"$cookieStore",
		"ajax",
		function($http, $location, $cookieStore, ajax) {

			return {
				login : function(data, successCb, errorCb) {
					ajax.doPost("/api/user/login", data, successCb, errorCb);
				},

				logout : function(successCb, errorCb) {
					ajax.doPost("/api/user/logout", {}, successCb, errorCb);
				},

				changePassword : function(data, successCb, errorCb) {
					ajax.doPost("/api/user/changepassword", data, successCb,
							errorCb);
				},
				loadRecords : function(data, successCb, errorCb) {
					ajax.doPost("/api/user/loadall", data, successCb, errorCb)
				}
			};
		} ]);

ct
		.factory(
				"staticInfoService",
				[
						"ajax",
						function(ajax) {
							return {
								loadRecords : function(recordType, successCb,
										errorCb) {
									ajax.doPost("/api/record/loadall", {
										recordType : recordType
									}, successCb, errorCb);
								},
								saveRecord : function(data, successCb, errorCb) {
									ajax.doPost("/api/record/create", data,
											successCb, errorCb);
								},

								deleteRecord : function(data, successCb,
										errorCb) {
									ajax.doPost("/api/record/delete", data,
											successCb, errorCb);
								},

								loadCasePlanGoalMajorCategories : function(
										successCb, errorCb) {
									ajax
											.doPost(
													"/api/record/loadcaseplangoalmajorcategories",
													{}, successCb, errorCb);
								},

								loadCasePlanGoalMinorCategories : function(
										majorCategoryID, successCb, errorCb) {
									ajax
											.doPost(
													"/api/record/loadcaseplangoalminorcategories",
													{
														majorCategoryID : majorCategoryID
													}, successCb, errorCb);
								}
							};
						} ]);

ct.factory("clientService", [ "ajax", function(ajax) {
	return {
		saveClient : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/save", data, successCb, errorCb);
		},
		loadClients : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/load", data, successCb, errorCb);
		},
		loadClientByID : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/load-by-id", data, successCb, errorCb);
		},
		loadAlerts : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/loadalerts", data, successCb, errorCb);
		},
		saveAlert : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/savealert", data, successCb, errorCb);
		},
		loadCaseNotes : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/loadcasenotes", data, successCb, errorCb);
		},
		saveCaseNote : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/savecasenote", data, successCb, errorCb);
		},
		loadCallout : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/loadcallout", data, successCb, errorCb);
		},
		saveCallout : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/savecallout", data, successCb, errorCb);
		},
		loadTrauma : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/loadtrauma", data, successCb, errorCb);
		},
		saveTrauma : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/savetrauma", data, successCb, errorCb);
		},
		loadCasePlans : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/loadcaseplans", data, successCb, errorCb);
		},
		saveCaseplan : function(data, successCb, errorCb) {
			ajax.doPost("/api/client/savecaseplan", data, successCb, errorCb);
		}

	};
} ]);

var LoginCtrl = [ "$scope", "$cookieStore", "$location", "userService",
		function($scope, $cookieStore, $location, userService) {

			if ($cookieStore.get("userInfo")) {
				$location.path("/clients");
			}

			$scope.user = {};
			$scope.login = function() {
				userService.login($scope.user, function(userInfo) {
					$cookieStore.put("userInfo", userInfo);
					$scope.user = {};
					$location.path("/clients");
				}, function(message) {
					alertError(message);
				});
			};
		} ];

var StaticInfoCtrl = [
		"$scope",
		"$routeParams",
		"staticInfoService",
		function($scope, $routeParams, staticInfoService) {

			$scope.staticRecordTypes = [ {
				description : "Alert Reasons",
				name : "AlertReason"
			}, {
				description : "Alert Types",
				name : "AlertType"
			}, {
				description : "Casenote Contact Types",
				name : "CaseNoteContactType"
			}, {
				description : "Casenote Outcomes",
				name : "CaseNoteOutcome"
			}, {
				description : "Casenote Reasons",
				name : "CaseNoteReason"
			}, {
				description : "Caseplan Closure Categories",
				name : "CasePlanClosureCategory"
			}, {
				description : "Client Statuses",
				name : "ClientStatus"
			}, {
				description : "Cultural Identities",
				name : "CulturalIdentity"
			}, {
				description : "Cultures",
				name : "Culture"
			}, {
				description : "Ethnicities",
				name : "Ethnicity"
			}, {
				description : "Genders",
				name : "IdentifiedGender"
			}, {
				description : "Offender Relationships",
				name : "OffenderRelationship"
			}, {
				description : "Sexualities",
				name : "Sexuality"
			}, {
				description : "Therapy Methods",
				name : "TherapyMethod"
			}, {
				description : "Training Types",
				name : "TrainingType"
			}, {
				description : "Homlessness Risks",
				name : "HomelessRisk"
			}, {
				description : "Drug Uses",
				name : "DrugUse"
			}, {
				description : "Caseplan Goal Categories",
				name : "CasePlanGoalCategory"
			} ];

			$scope.setCurrentRecordType = function(recordType) {
				$scope.currentRecordType = recordType;
				$scope.loadRecords();
			};

			$scope.records = [];

			$scope.loadRecords = function() {
				if ($scope.currentRecordType) {
					if ($scope.currentRecordType.name == "CasePlanGoalCategory") {
						staticInfoService
								.loadCasePlanGoalMajorCategories(
										function(records) {
											$scope.records = records;
											if ($scope.currentCasePlanGoalMajorCategory) {
												$scope
														.loadCasePlanGoalMinorCategories();
											}
										}, function(message) {
											alertError(message);
										});
					} else {

						if ($scope.currentRecordType) {
							staticInfoService.loadRecords(
									$scope.currentRecordType.name, function(
											records) {
										$scope.records = records;
									}, function(message) {
										alertError(message);
									});
						}
					}
				}
			};
			$scope.loadRecords();

			$scope.casePlanGoalMinorCategories = [];
			$scope.loadCasePlanGoalMinorCategories = function() {
				staticInfoService.loadCasePlanGoalMinorCategories(
						$scope.currentCasePlanGoalMajorCategory.id, function(
								records) {
							$scope.casePlanGoalMinorCategories = records;
						}, function(message) {
							alertError(message);
						});
			};

			$scope.currentRecord = {};

			$scope.currentCasePlanGoalMajorCategory = null;

			$scope.createNewRecord = function() {
				$scope.currentRecord = {};
				$scope.showRecordModal = true;
			};

			$scope.createNewCasePlanGoalCategory = function() {
				$scope.currentRecord = {};
				$scope.showCasePlanGoalCategoryModal = true;
			};

			$scope.selectRecord = function(record) {
				$scope.currentRecord = record;
				$scope.currentCasePlanGoalMajorCategory = record;
				$scope.loadCasePlanGoalMinorCategories(record.id);
			};

			$scope.saveRecord = function() {
				var postData = {};
				if (!$scope.currentRecord.active) {
					$scope.currentRecord.active = false;
				}
				if ($scope.currentRecord.id) {
					postData.id = $scope.currentRecord.id;
					delete $scope.currentRecord.id;
				}
				if ($scope.currentRecord.majorCategory) {
					$scope.currentRecord.majorCategory_id = $scope.currentRecord.majorCategory.id;
					delete $scope.currentRecord.majorCategory;
				}
				postData.recordType = $scope.currentRecordType.name;
				postData.record = $scope.currentRecord;
				staticInfoService.saveRecord(postData, function() {
					alertSuccess("Record saved");
					$scope.cancelRecord();
					$scope.cancelCasePlanGoalCategory();
					$scope.loadRecords();
				}, function(message) {
					alertError(message);
				});
			};

			$scope.cancelRecord = function() {
				$scope.currentRecord = {};
				$scope.showRecordModal = false;
				$scope.loadRecords();
			};

			$scope.cancelCasePlanGoalCategory = function() {
				$scope.currentRecord = {};
				$scope.showCasePlanGoalCategoryModal = false;
				$scope.loadRecords();
			};

			$scope.deleteRecord = function(record) {
				if (window.confirm("Are you sure you want to delete '"
						+ record.name + "'?")) {
					staticInfoService.deleteRecord({
						recordType : $scope.currentRecordType.name,
						id : record.id
					}, function() {
						alertSuccess("Record deleted.");
						$scope.loadRecords();
					}, function(message) {
						alertError(message);
					});
				}
			};

			$scope.editRecord = function(record) {
				$scope.currentRecord = record;
				$scope.showRecordModal = true;
			};

			$scope.editCasePlanGoalCategory = function(record) {
				$scope.currentRecord = record;
				if (record.majorcategory_id) {
					$scope.currentRecord.majorCategory = getMajorCategory(record.majorcategory_id);
				}
				$scope.showCasePlanGoalCategoryModal = true;
			};

			var getMajorCategory = function(id) {
				var result = null;
				angular.forEach($scope.records, function(record) {
					if (record.id == id) {
						result = record;
					}
				});
				return result;
			};

		} ];

var ClientsCtrl = [
		"$scope",
		"$routeParams",
		"$location",
		"clientService",
		function($scope, $routeParams, $location, clientService) {

			$scope.newClient = {};
			$scope.createNewClient = function() {
				$scope.showNewClientModal = true;
				$scope.newClient = {};
			};

			$scope.cancelNewClient = function() {
				$scope.newClient = {};
				$scope.showNewClientModal = false;
			};

			$scope.saveNewClient = function() {
				clientService.saveClient($scope.newClient, function() {
					alertSuccess("Client saved.");
					$scope.showNewClientModal = false;
					$scope.newClient = {};
				}, function(message) {
					alertError(message);
				});
			};

			$scope.queryParams = {};
			$scope.queryParams.substring = $routeParams.substring || "";
			$scope.queryParams.mineOnly = $routeParams.mine;
			$scope.queryParams.samssaOnly = $routeParams.samssa;
			$scope.clients = [];
			$scope.recordCount = 0;
			$scope.queryParams.limit = 10;
			$scope.queryParams.offset = ($routeParams.page - 1)
					* $scope.queryParams.limit || 0;
			$scope.loadClients = function() {
				clientService.loadClients($scope.queryParams, function(result) {
					$scope.clients = result.records;
					$scope.recordCount = result.count;
				}, function(message) {
					alertError(message);
				});
			};
			$scope.loadClients();

			// paging stuff
			$scope.currentPage = $routeParams.page || 1;
			$scope.getPages = function() {
				var pages = [];
				if ($scope.recordCount == 0) {
					return [];
				}
				for (var i = 1; i < Math.ceil($scope.recordCount
						/ $scope.queryParams.limit) + 1; i++) {
					pages.push(i);
				}
				return pages;
			};

			$scope.movePrevious = function() {
				if ($scope.currentPage > 1) {
					$scope.goToPage($scope.currentPage - 1);
				}
			};

			$scope.moveNext = function() {
				if ($scope.currentPage < Math.ceil($scope.recordCount
						/ $scope.queryParams.limit)) {
					$scope.goToPage($scope.currentPage + 1);
				}
				;
			};

			$scope.loadPage = function() {
				var urlParams = {};
				if ($scope.currentPage) {
					urlParams.page = $scope.currentPage;
				}
				;
				if ($scope.queryParams.substring) {
					urlParams.substring = $scope.queryParams.substring;
				}
				;
				if ($scope.queryParams.mineOnly) {
					urlParams.mine = $scope.queryParams.mineOnly;
				}
				;
				if ($scope.queryParams.samssaOnly) {
					urlParams.samssa = $scope.queryParams.samssaOnly;
				}
				;
				$location.search(urlParams);
			};

			$scope.goToPage = function(page) {
				$scope.currentPage = page;
				$scope.loadPage();
			};

			$scope.doSearch = function() {
				$scope.currentPage = 1;
				$scope.loadPage();
			};

			$scope.selectClient = function(client) {
				$location.path("/clients/" + client.id);
			};

		} ];

var ClientCtrl = [
		"$scope",
		"$routeParams",
		"$location",
		"clientService",
		"staticInfoService",
		"userService",
		function($scope, $routeParams, $location, clientService,
				staticInfoService, userService) {

			$scope.clientID = parseInt($routeParams.clientID) || 0;

			// static info
			$scope.loadStaticInfo = function() {
				$scope.identifiedGenders = [];
				staticInfoService.loadRecords("IdentifiedGender", function(
						records) {
					$scope.identifiedGenders = records;
					$scope.client.identifiedgender_id = findRecord(records,
							$scope.client.identifiedgender_id);
				});

				$scope.contactRelations = [ 'Primary victim/survivor',
						'Partner of victim/survivor',
						'Friend of victim/survivor',
						'Parent of victim/survivor',
						'Other family member of victim/survivor',
						'Carer/supporter of victim/survivor',
						'Professional/organisational representative' ];

				$scope.clientStatuses = [];
				staticInfoService.loadRecords("ClientStatus",
						function(records) {
							$scope.clientStatuses = records;
							$scope.client.status_id = findRecord(records,
									$scope.client.status_id);
						});

				$scope.staffs = [];
				userService.loadRecords({}, function(records) {
					$scope.staffs = records;
					$scope.client.counsellor_id = findRecord(records,
							$scope.client.counsellor_id);
				});

				$scope.homelessRisks = [];
				staticInfoService.loadRecords("HomelessRisk",
						function(records) {
							$scope.homelessRisks = records;
							$scope.client.homelessrisk_id = findRecord(records,
									$scope.client.homelessrisk_id);
						});

				$scope.cultures = [];
				staticInfoService.loadRecords("Culture", function(records) {
					$scope.cultures = records;
					$scope.client.culture_id = findRecord(records,
							$scope.client.culture_id);
				});

				$scope.states = [ 'ACT', 'NSW', 'VIC', 'QLD', 'SA', 'WA', 'NT',
						'TAS', 'Overseas' ];

				$scope.ethnicities = [];
				staticInfoService.loadRecords("Ethnicity", function(records) {
					$scope.ethnicities = records;
					$scope.client.ethnicity_id = findRecord(records,
							$scope.client.ethnicity_id);
				});

				$scope.sexualities = [];
				staticInfoService.loadRecords("Sexuality", function(records) {
					$scope.sexualities = records;
					$scope.client.identifiedsexuality_id = findRecord(records,
							$scope.client.identifiedsexuality_id);
				});

				$scope.alertParams.clientID = $scope.client.id;
				$scope.loadAlerts($scope.alertParams);

				$scope.alertTypes = [];
				staticInfoService.loadRecords("AlertType", function(records) {
					$scope.alertTypes = records;
				});

				$scope.alertReasons = [];
				staticInfoService.loadRecords("AlertReason", function(records) {
					$scope.alertReasons = records;
				});

				$scope.loadCaseNotes();

				staticInfoService.loadRecords('CaseNoteReason', function(
						records) {
					$scope.caseNoteReasons = records;
				});

				staticInfoService.loadRecords('CaseNoteOutcome', function(
						records) {
					$scope.caseNoteOutcomes = records;
				});

				staticInfoService.loadRecords('CaseNoteContactType', function(
						records) {
					$scope.caseNoteContactTypes = records;
				});

				$scope.loadCasePlans();

				staticInfoService.loadRecords('CasePlanClosureCategory',
						function(records) {
							$scope.casePlanClosureCategories = records;
						}, function(message) {
							alertError(message);
						});
			};

			$scope.loadCallout = function() {
				clientService.loadCallout({
					clientID : $scope.client.id
				}, function(callout) {
					if (callout) {
						$scope.currentCallout = callout;
					} else {
						$scope.currentCallout = {};
					}
				}, function(error) {
					alertError(error);
				});
			};

			$scope.trauma = {};
			$scope.loadTrauma = function() {
				clientService.loadTrauma({
					clientID : $scope.client.id
				}, function(trauma) {
					if (trauma) {
						$scope.trauma = trauma;
					} else {
						$scope.trauma = {};
					}
				}, function(msg) {
					alertError(msg);
				});
			};

			$scope.caseplans = [];
			$scope.loadCasePlans = function() {
				clientService.loadCasePlans({
					clientID : $scope.client.id
				}, function(caseplans) {
					$scope.caseplans = caseplans;
				}, function(message) {
					alertError(message);
				});
			};

			if ($scope.clientID == 0) {
				$location.path("/clients");
			}

			$scope.client = null;
			$scope.loadClient = function() {
				clientService.loadClientByID({
					clientID : $scope.clientID
				}, function(client) {
					$scope.client = client;
					$scope.loadStaticInfo();
					$scope.loadCallout();
					$scope.loadTrauma();
				}, function(errorMsg) {
					alertError(errorMsg);
				});
			};
			$scope.loadClient();

			$scope.currentView = 'summary';

			$scope.saveClient = function() {
				console.log($scope.client);
				var updatedClient = angular.copy($scope.client);
				updatedClient.identifiedgender_id = $scope.client.identifiedgender_id ? $scope.client.identifiedgender_id.id
						: null;
				updatedClient.status_id = $scope.client.status_id ? $scope.client.status_id.id
						: null;
				updatedClient.counsellor_id = $scope.client.counsellor_id ? $scope.client.counsellor_id.id
						: null;
				updatedClient.homelessrisk_id = $scope.client.homelessrisk_id ? $scope.client.homelessrisk_id.id
						: null;
				updatedClient.culture_id = $scope.client.culture_id ? $scope.client.culture_id.id
						: null;
				updatedClient.ethnicity_id = $scope.client.ethnicity_id ? $scope.client.ethnicity_id.id
						: null;
				updatedClient.identifiedsexuality_id = $scope.client.identifiedsexuality_id ? $scope.client.identifiedsexuality_id.id
						: null;
				clientService.saveClient(updatedClient, function(data) {
					alertSuccess("Client saved!");
					$scope.loadClient();
				}, function(message) {
					alertError(message);
				});
			};

			$scope.resetClient = function() {
				if (window.confirm("Are you sure you want to reset?")) {
					$scope.loadClient();
				}
			};

			// alerts
			$scope.alertParams = {};
			$scope.alerts = [];
			$scope.loadAlerts = function() {
				clientService.loadAlerts($scope.alertParams, function(records) {
					$scope.alerts = records;
				}, function(message) {
					alertError(message);
				});
			};

			$scope.createNewAlert = function() {
				$scope.currentAlert = {
					active : true
				};
			};

			$scope.cancelAlert = function() {
				if (window.confirm("Are you sure you want to cancel?")) {
					$scope.createNewAlert();
				}
			};

			$scope.saveAlert = function() {
				console.log($scope.currentAlert);
				var updatedAlert = angular.copy($scope.currentAlert);
				updatedAlert.alerttype_id = $scope.currentAlert.alerttype_id ? $scope.currentAlert.alerttype_id.id
						: null;
				updatedAlert.alertreason_id = $scope.currentAlert.alertreason_id ? $scope.currentAlert.alertreason_id.id
						: null;
				updatedAlert.client_id = $scope.client.id;
				clientService.saveAlert(updatedAlert, function(data) {
					alertSuccess("Alert saved!");
					$scope.loadAlerts();
					$scope.createNewAlert();
				}, function(message) {
					alertError(message);
				});
			};

			$scope.selectAlert = function(alert) {
				$scope.currentAlert = angular.copy(alert);
				$scope.currentAlert.alerttype_id = findRecord(
						$scope.alertTypes, alert.alerttype_id);
				$scope.currentAlert.alertreason_id = findRecord(
						$scope.alertReasons, alert.alertreason_id);
				delete $scope.currentAlert.alertType;
			};

			// casenotes
			$scope.caseNoteParams = {};
			$scope.caseNotes = [];
			$scope.loadCaseNotes = function() {
				$scope.caseNoteParams.clientID = $scope.client.id;
				clientService.loadCaseNotes($scope.caseNoteParams, function(
						records) {
					$scope.caseNotes = records;
				}, function(message) {
					alertError(message);
				});
			};

			$scope.currentCaseNote = {};
			$scope.createCaseNote = function() {
				$scope.currentCaseNote = {};
			};
			$scope.selectCaseNote = function(caseNote) {
				$scope.currentCaseNote = caseNote;
				$scope.currentCaseNote.reason_id = findRecord(
						$scope.caseNoteReasons,
						$scope.currentCaseNote.reason_id);
				$scope.currentCaseNote.contacttype_id = findRecord(
						$scope.caseNoteContactTypes,
						$scope.currentCaseNote.contacttype_id);
				$scope.currentCaseNote.outcome_id = findRecord(
						$scope.caseNoteOutcomes,
						$scope.currentCaseNote.outcome_id);
			};

			$scope.caseNoteMinutes = [ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50,
					55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115,
					120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175,
					180 ];
			$scope.caseNoteReasons = [];
			$scope.caseNoteContactTypes = [];
			$scope.saveCaseNote = function() {
				var updatedCaseNote = angular.copy($scope.currentCaseNote);
				updatedCaseNote.reason_id = $scope.currentCaseNote.reason_id ? $scope.currentCaseNote.reason_id.id
						: null;
				updatedCaseNote.outcome_id = $scope.currentCaseNote.outcome_id ? $scope.currentCaseNote.outcome_id.id
						: null;
				updatedCaseNote.contacttype_id = $scope.currentCaseNote.contacttype_id ? $scope.currentCaseNote.contacttype_id.id
						: null;
				updatedCaseNote.client_id = $scope.client.id;
				clientService.saveCaseNote(updatedCaseNote, function() {
					alertSuccess("Case Note saved!");
					$scope.createCaseNote();
					$scope.loadCaseNotes();
				}, function(msg) {
					alertError(msg);
				});
			};

			$scope.saveCallout = function() {
				$scope.currentCallout.client_id = $scope.client.id;
				clientService.saveCallout($scope.currentCallout, function() {
					$scope.loadCallout();
					alertSuccess("Callout Saved!");
				}, function(msg) {
					alertError(msg);
				});
			};

			$scope.cancelCallout = function() {
				$scope.loadCallout();
			};

			$scope.saveTrauma = function() {
				var updatedTrauma = angular.copy($scope.trauma);
				updatedTrauma.client_id = $scope.client.id;
				clientService.saveTrauma(updatedTrauma, function() {
					alertSuccess("Record Saved!");
				}, function(errorMessage) {
					alertError(errorMessage);
				});
			};

			$scope.cancelTrauma = function() {
				$scope.loadTrauma();
			};

			$scope.currentCaseplan = {};
			$scope.saveCaseplan = function() {
				var updatedCaseplan = angular.copy($scope.currentCaseplan);
				updatedCaseplan.closurecategory_id = $scope.currentCaseplan.closurecategory_id ? $scope.currentCaseplan.closurecategory_id.id
						: null;
				updatedCaseplan.client_id = $scope.client.id;
				clientService.saveCaseplan(updatedCaseplan, function() {
					alertSuccess("Caseplan saved!");
					$scope.loadCasePlans();
					$scope.showNewCasePlan = false;
				}, function(message) {
					alertError(message);
				});

			};

			$scope.cancelCaseplan = function() {
				$scope.currentCaseplan = {};
				$scope.showNewCasePlan = false;
			};

		} ];
