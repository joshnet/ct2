(ns ct2.data
 (:require [clojure.java.jdbc :as sql]))

 (def db-spec
   {:subprotocol "mysql"
    :subname "//localhost:3306/casetracklive"
    :user "root"
    :password ""})

 (defn save-record
  [table-name record]
  (:generated_key (first (sql/insert! db-spec table-name record))))

(defn update-record
  [table-name record id]
  (first (sql/update! db-spec table-name record ["id = ?" id])))

(defn update-record-by-column
  [table-name record column-name column-value]
  (first (sql/update! db-spec table-name record [(str column-name " = ?") column-value])))

(defn delete-record
  [table-name record-id]
  (sql/delete! db-spec table-name ["id = ?" record-id]))

(defn get-record-by-id
  [table-name record-id]
  (first (sql/query db-spec [(str "SELECT * FROM " table-name " WHERE id = ?") record-id])))

(defn get-all-records
  ([table-name limit offset]
    (sql/query db-spec [(str "SELECT * FROM " table-name " LIMIT ? OFFSET ?") limit offset]))
  ([table-name]
    (get-all-records table-name 100 0)))

(defn get-records-by-sql
  [sql & params]
  (sql/query db-spec (concat [sql] (or params []))))
