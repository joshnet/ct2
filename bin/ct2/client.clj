(ns ct2.client
  (:use ct2.data
        ct2.dateutils
        ct2.utils)
  (:require [ct2.user :as user]))

(def page-size 50)

(defn parse-dates
  [client date-fields]
  (loop [fields date-fields updated-client client]
    (if (empty? fields)
      updated-client
      (recur
       (rest fields)
       (merge updated-client
              {(first fields) (date-to-string ((first fields) updated-client))})))))

(defn- client-dto
  [client]
  (merge (parse-dates client
                      [:famsacdate
                       :adultsexualassultdate
                       :assultbymultipleoffendersdate
                       :assultedat1225date
                       :childsexualassultdate
                       :dateofbirth
                       :dateofcontact
                       :domesticviolencedate
                       :drugalcoholfacilitatedsadate
                       :firstcontactdate
                       :firstdisclosuredate
                       :incidentsviaelectronicmediadate
                       :otherassultdate
                       :recentincident
                       :recentlyassulteddate
                       :sexualharrassmentdate
                       :specifyotherassultdate
                       :stalkingdate
                       :touchingoffencesdate
                       :violencebyintimatepartnerdate
                       :witnesssexualoffencedate])))

(defn- alert-dto
  [alert]
  (merge alert
         {:alertType (:name (get-record-by-id "AlertType" (:alerttype_id alert)))}))


(defn- case-note-dto
  [casenote]
  (merge casenote {:dateofservice (date-to-string (:dateofservice casenote))
                   :contactType (:name (get-record-by-id "CaseNoteContactType" (:contacttype_id casenote)))}))

(defn- caseplan-dto
  [caseplan]
  (merge caseplan 
         {:counsellor (:username (get-record-by-id "Staff" (:createdby_id caseplan)))}))

(defn generate-client-load-whereclause
  [params]
  (str
   "(firstName LIKE ? OR otherName LIKE ? OR lastName LIKE ? OR fileNumber LIKE ? OR dateOfBirth LIKE ? ) "
   (if (:mineOnly params) " AND counsellor_id = ? ")
   (if (:samssaOnly params) " AND samssa = ?")))

(defn generate-client-load-params
  [params]
  (let [substr (if (:substring params) (str "%" (:substring params) "%") "%")
        staffID (:staff_id (user/get-session-by-session-key (:sessionKey params)))]
    (filter #(not (nil? %))
            (vector substr substr substr substr substr
                    (if (:mineOnly params) staffID nil)
                    (if (:samssaOnly params) true nil)))))

(defn load-clients
  [params]
  (let [query (str "SELECT * FROM Client WHERE "
                   (generate-client-load-whereclause params)
                   (str " LIMIT ? OFFSET ? "))
        query-params (generate-client-load-params params)]
    (apply get-records-by-sql (concat [query] query-params [(or (:limit params) page-size) (or (:offset params) 0)]))))

(defn count-clients
  [params]
  (let [query (str "SELECT COUNT(*) as count FROM Client WHERE " (generate-client-load-whereclause params))
        params (generate-client-load-params params)]
    (:count (first (apply get-records-by-sql (concat [query] params))))))

(defn get-clients
  [params]
  (pass {:records (map client-dto (load-clients params))
         :count (count-clients params)}))

(defn create-client
  [client]
  (merge (dissoc client :sessionKey :age)
         {:dateofbirth (string-to-timestamp (:dateofbirth client))
          :dateofcontact (string-to-timestamp (:dateofcontact client))
          :firstcontactdate (string-to-timestamp (:firstcontactdate client))
          :sacat (or (:sacat client) false),
          :aboriginalcounsellorpreffered (or (:aboriginalcounsellorpreffered client) false)
          :adultsexualassult (or (:adultsexualassult client) false)
          :alcoholuse (or (:alcoholuse client) false)
          :assultbymultipleoffenders (or (:assultbymultipleoffenders client) false)
          :assultedat1225 (or (:assultedat1225 client) false)
          :cancontactexternalservices (or (:cancontactexternalservices client) false)
          :canleavemessage (or (:canleavemessage client) false)
          :cansendletters (or (:cansendletters client) false)
          :cantext (or (:cantext client) false)
          :chargeslaid (or (:chargeslaid client) false)
          :childsexualassult (or (:childsexualassult client) false)
          :counsellorallocated (or (:counsellorallocated client) false)
          :crcc (or (:crcc client) false)
          :difficulthearing (or (:difficulthearing client) false)
          :difficultremembering (or (:difficultremembering client) false)
          :difficultseeing (or (:difficultseeing client) false)
          :difficultwalking (or (:difficultwalking client))
          :disability (or (:disability client) false)
          :domesticviolence (or (:domesticviolence client) false)
          :drugalcoholfacilitatedsa (or (:drugalcoholfacilitatedsa client) false)
          :druguse (or (:druguse client) false)
          :eatingproblems (or (:eatingproblems client) false)
          :forensicexamconducted (or (:forensicexamconducted client) false)
          :formalstatement (or (:formalstatement client) false)
          :generaldutiespolice (or (:generaldutiespolice client) false)
          :incidentsviaelectronicmedia (or (:incidentsviaelectronicmedia client) false)
          :incompletestatement (or (:incompletestatement client) false)
          :informalstatement (or (:informalstatement client) false)
          :interpreterneeded (or (:interpreterneeded client) false)
          :ischildatrisk (or (:ischildatrisk client) false)
          :medicalexamconducted (or (:medicalexamconducted client) false)
          :meetandgreet (or (:meetandgreet client) false)
          :mentalhealth (or (:mentalhealth client) false)
          :nguru (or (:nguru client) false)
          :otherexam (or (:otherexam client) false)
          :otherhealth (or (:otherhealth client) false)
          :otherpolice (or (:otherpolice client) false)
          :otherstatement (or (:otherstatement client) false)
          :pendingfurtherinvestigation (or (:pendingfurtherinvestigation client) false)
          :policenotified (or (:plicenotified client) false)
          :recentlyassulted (or (:recentlyassulted client) false)
          :reporttocare (or (:reporttocare client) false)
          :samssa (or (:samssa client) false)
          :selfharm (or (:selfharm client) false)
          :sexualharrassment (or (:sexualharrassment client) false)
          :stalking (or (:stalking client) false)
          :suicidal (or (:suicidal client) false)
          :touchingoffences (or (:touchingoffences client) false)
          :violencebyintimatepartner (or (:violencebyintimatepartner client) false)
          :witnesssexualoffence (or (:witnesssexualoffence client) false)
          :wraparound (or (:wraparound client) false)
          :counselloronly (or (:counselloronly client) false)
          :postcode 2600
          :recentincident (string-to-date (:recentincident client))
          :recentlyassulteddate (string-to-date (:recentlyassulteddate client))
          :assultedat1225date (string-to-date (:assultedat1225date client))
          :drugalcoholfacilitatedsadate (string-to-date (:drugalcoholfacilitatedsadate client))
          :incidentsviaelectronicmediadate (string-to-date (:incidentsviaelectronicmedia client))
          :touchingoffencesdate (string-to-date (:touchingoffencesdate client))
          :childsexualassultdate (string-to-date (:childsexualassultdate client))
          :adultsexualassultdate (string-to-date (:adultsexualassultdate client))
          :assultbymultipleoffendersdate (string-to-date (:assultbymultipleoffendersdate client))
          :violencebyintimatepartnerdate (string-to-date (:violencebyintimatepartnerdate client))
          :sexualharrassmentdate (string-to-date (:sexualharrassmentdate client))
          :stalkingdate (string-to-date (:stalkingdate client))
          :witnesssexualoffencedate (string-to-date (:witnesssexualoffencedate client))
          :domesticviolencedate (string-to-date (:domesticviolencedate client))
          :otherassult (or (:otherassult client) false)
          :otherassultdate (string-to-date (:otherassultdate client))
          :firstdisclosuredate (string-to-date (:firstdisclosuredate client))
          :famsacdate (string-to-date (:famsacdate client))
          :specifyotherassultdate (string-to-date (:specifyotherassultdate client))}))

(defn save
  [client]
  (cond
   (not (or (:firstName client) (:firstname client))) (fail "You must provide firstName")
   (not (or (:lastName client) (:lastname client))) (fail "You must provide lastName")
   (is-future-date (:dateofbirth client)) (fail "Date of birth cannot be in the future")
   (is-future-date (:firstcontactdate client)) (fail "First contact date cannot be in the future")
   :else
   (if (:id client)
     (pass (update-record "Client" (dissoc (create-client client) :id) (:id client)))
     (pass (save-record "Client" (create-client client))))))

(defn load-by-id
  [client-id]
  (if (not client-id)
    (fail "You must provide clientID")
    (pass (client-dto (get-record-by-id "Client" client-id)))))

(defn load-alerts
  [client-id substring active-only]
  (let [substr (if substring (str "%" substring "%") "%")
        active (or active-only false)]
    (pass (map alert-dto (get-records-by-sql
                          "SELECT * FROM Alert WHERE (active = ? OR active = ?)  AND
issue LIKE ? AND client_id = ? ORDER BY active  DESC, id DESC"
                          true active substr client-id)))))


(defn save-alert
  [alert]
  (cond
   (nil? (:issue alert)) (fail "You must provide the alert description")
   :else
   (if (:id alert)
     (pass (update-record "Alert" (dissoc (merge alert {:permanent (or (:permanent alert) false)}) :id) (:id alert)))
     (pass (save-record "Alert" (merge alert {:permanent (or (:permanent alert) false)}))))))

(defn load-case-notes
  [client-id substr]
  (let [substring (if substr (str "%" substr "%") "%")]
    (pass (map case-note-dto (get-records-by-sql
                              "SELECT * FROM CaseNote WHERE client_id = ? AND note LIKE ? ORDER BY dateofservice DESC"
                              client-id substring)))))


(defn prepare-casenote
  [casenote]
  (merge (dissoc casenote :contactType :datecreated)
         {:dateofservice (string-to-timestamp (:dateofservice casenote))}))

(defn save-case-note
  [casenote]
  (if (:id casenote)
    (pass (update-record "CaseNote" (prepare-casenote (dissoc casenote :id)) (:id casenote)))
    (pass (save-record "CaseNote" (merge (prepare-casenote casenote)
                                         {:datecreated (get-current-timestamp)})))))

(defn prepare-callout
  [callout]
  (merge callout
         {:sacat (or (:sacat callout) false)
          :cancontactexternalservices (or (:cancontactexternalservices callout) false)
          :chargeslaid (or (:chargeslaid callout) false)
          :forensicexamconducted (or (:forensicexamconducted callout) false)
          :formalstatement (or (:formalstatement callout) false)
          :generaldutiespolice (or (:generaldutiespolice callout) false)
          :incompletestatement (or (:incompletestatement callout) false)
          :informalstatement (or (:informalstatement callout) false)
          :ischildatrisk (or (:ischildatrisk callout) false)
          :medicalexamconducted (or (:medicalexamconducted callout) false)
          :meetandgreet (or (:meetandgreet callout) false)
          :otherexam (or (:otherexam callout) false)
          :otherpolice (or (:otherpolice callout) false)
          :otherstatment (or (:otherstatment callout) false)
          :pendingfurtherinvestigation (or (:pendingfurtherinvestigation callout) false)
          :policenotified (or (:policenotified callout) false)
          :reporttocare (or (:reporttocare callout) false)
          :dateofcontact (string-to-date (:dateofcontact callout))
          :famsacdate (string-to-date (:famsacdate callout))}))

(defn save-callout
  [callout]
  (if (:id callout)
    (pass (update-record "Callout" (dissoc (prepare-callout callout) :id) (:id callout)))
    (pass (save-record "Callout" (prepare-callout callout)))))

(defn load-callout
  [clientID]
  (pass (first (get-records-by-sql "SELECT * FROM Callout WHERE client_id = ?" clientID))))

(defn load-trauma
  [clientID]
  (pass (first (get-records-by-sql "SELECT * FROM Trauma WHERE client_id = ? " clientID))))

(defn save-trauma
  [trauma]
  (if (:id trauma)
    (pass (update-record "Trauma" (dissoc trauma :id) (:id trauma)))
    (pass (save-record "Trauma" trauma))))

(defn load-caseplans
  [clientID]
  (pass  (map caseplan-dto (get-records-by-sql "SELECT * FROM CasePlan WHERE client_id = ?" clientID))))


(defn save-caseplan
  [caseplan]
  (if (:id caseplan)
    (pass (update-record "CasePlan" (dissoc caseplan :id) (:id caseplan)))
    (pass (save-record "CasePlan" caseplan))))

;(defn create-100-clients
;  []
;  (let [session-key (user-login {:usernme "Josh" :password "pass"})
;        firstNames ["John","Peter", "Paul", "James", "Simon", "David", "Daniel", "Joseph", "Jacob" ]
;        lastNames ["Smith", "May", "Clackson", "Jackson", "Bush", "Green", "Elton", "Bell"]
;        otherNames ["Gray", "Graham", "Kowalsky", "Watson", "Martin", "Lawrence", "Simpson"]]
;    (doseq [x (range 101)]
;      (let [client {:firstName (get firstNames (rand-int (count firstNames)))
;                    :lastName (get lastNames (rand-int (count lastNames)))
;                    :otherName (get otherNames (rand-int (count otherNames)))
;                    :fileNumber (rand-int 10000)
;                    :dateOfBirth (str (inc (rand-int 27)) "/"
;                                      (inc (rand-int 11)) "/"
;                                      (+ 1900 (rand-int 100)))}]
;        (client-save client)))))

;(create-100-clients)
