(ns ct2.service
  (:use ct2.dateutils
        ct2.data
        ct2.static-info
        ct2.utils)
  (:require [clojure.java.jdbc :as sql]
            [clostache.parser :as tpl]
            [ct2.user :as user]
            [ct2.client :as client])
  (:import [java.sql Timestamp]))

(defn record-create
  [params]
  (static-record-create (:recordType params) (:record params) (:id params)))

(defn record-delete
  [params]
  (static-record-delete (:recordType params) (:id params)))

(defn record-loadall
  [params]
  (static-record-load (:recordType params) (:limit params) (:offset params)))

(defn record-loadcaseplangoalmajorcategories
  [params]
  (load-caseplan-goal-major-categories))

(defn record-loadcaseplangoalminorcategories
  [params]
  (load-caseplan-goal-minor-categories (:majorCategoryID params)))

(defn user-login
  [params]
  (user/login (:username params) (:password params)))

(defn user-logout
  [params]
  (user/logout (:sessionKey params)))

(defn user-changepassword
  [params]
  (user/change-password
    (:oldPassword params)
    (:newPassword params)
    (:newPasswordConfirmation params)
    (:sessionKey params)))

(defn user-loadall
  [params]
  (user/load-all-users))
                                        ;CLIENT API

(defn client-load
  [params]
  (client/get-clients params))

(defn client-save
  [params]
  (client/save params))

(defn client-load-by-id
  [params]
  (client/load-by-id (:clientID params)))

(defn client-loadalerts
  [params]
  (client/load-alerts (:clientID params) (:substring params) (:activeOnly params)))

(defn client-savealert
  [params]
  (client/save-alert (dissoc params :sessionKey)))

(defn client-loadcasenotes
  [params]
  (client/load-case-notes (:clientID params) (:substring params)))

(defn client-savecasenote
  [params]
  (client/save-case-note (dissoc params :sessionKey)))

(defn client-savecallout
  [params]
  (client/save-callout (dissoc params :sessionKey)))

(defn client-loadcallout
  [params]
  (client/load-callout (:clientID params)))

(defn client-loadtrauma
  [params]
  (client/load-trauma (:clientID params)))

(defn client-savetrauma
  [params]
  (client/save-trauma (dissoc params :sessionKey)))

(defn client-loadcaseplans
  [params]
  (client/load-caseplans (:clientID params)))

(defn client-savecaseplan
  [params]
  (client/save-caseplan (dissoc params :sessionKey)))
