(ns ct2.utils)

(defn pass
  [data]
  {:success true :data data})

(defn fail
  [message]
  {:success false :message message})