(ns ct2.handler
  (:use compojure.core ct2.service ct2.user)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [cheshire.core :as json]
            [clojure.java.io :as io]))

(defn get-params
  [request]
  (json/parse-string (slurp (:body request)) true))

(defn get-service-name
  [uri]
  (println "Getting service::" + uri)
  (let [uri-parts (clojure.string/split uri #"\/")]
    (str (get uri-parts 2) "-" (get uri-parts 3))))

(defroutes app-routes
  (GET "/" request
       {:body (io/input-stream (io/resource "public/index.html"))
        :status 200
        :headers {"Content-Type" "text/html"}})

  ;API HANDLER
  (POST "/api/*" request
        (try
          (let [service-func (resolve (symbol "ct2.service" (get-service-name (:uri request))))
                params (get-params request)]
            (println params)
            (if (or (= (:uri request) "/api/user/login")
                    (= (:uri request) "/api/user/logout"))
              (json/generate-string (service-func params))
              (if (check-session-key (:sessionKey params))
                (json/generate-string (service-func params))
                (json/generate-string {:success false :message "Invalid session key"}))))
          (catch Exception e
            (.printStackTrace e)
            (json/generate-string {:success false :message (.getMessage e)}))))

  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (handler/site app-routes))
