(ns ct2.dateutils
  (:import [java.sql Timestamp]
           [java.text SimpleDateFormat]))

(def DEFAULT_DATE_FORMAT "dd/MM/yyyy")

(defn string-to-date
  [datestr]
  (try 
    (let [sdf (SimpleDateFormat. DEFAULT_DATE_FORMAT)]
      (.parse sdf datestr))
    (catch Exception e 
      nil)))

(defn string-to-timestamp
  [datestr]
  (if datestr 
    (let [date (string-to-date datestr)]
      (if date 
        (Timestamp. (.getTime date))
        nil))))

(defn get-current-timestamp
  []
  (Timestamp. (java.lang.System/currentTimeMillis)))

(defn date-to-string
  [d]
  (cond 
    (nil? d) ""
    :else 
    (let [sdf (SimpleDateFormat. DEFAULT_DATE_FORMAT)]
      (.format sdf d))))

(defn age 
  [dob]
  (try
    (let [sdf (SimpleDateFormat. "yyyy")
          yob (.format sdf dob)
          current-year (.format sdf (get-current-timestamp))]
      (- (Integer. current-year) (Integer. yob)))
    (catch Exception e
      0)))

(defn is-future-date 
  [date-str]
  (> (.getTime (string-to-timestamp date-str)) (.getTime (get-current-timestamp))))

