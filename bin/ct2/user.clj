(ns ct2.user
  (:use ct2.data ct2.dateutils ct2.utils))

(defn get-user-by-username
  [username]
  (first (get-records-by-sql "SELECT * FROM Staff WHERE username = ?"  username)))

(defn check-password
  [user password]
  (= (:password user) password))

(defn kill-sessions
  [user]
  (update-record-by-column "UserSession"
                 {:active false :dateLoggedOut (get-current-timestamp)}
                 "staff_id" (:id user)))

(defn create-user-session
  [user]
  (kill-sessions user)
  (let [sessionKey (.toString (java.util.UUID/randomUUID))]
    (save-record "UserSession"
                 {:active true
                  :dateLoggedIn  (get-current-timestamp)
                  :sessionKey sessionKey
                  :staff_id (:id user)})
    sessionKey))

(defn get-session-by-session-key
  [session-key]
  (first (get-records-by-sql "SELECT * FROM UserSession WHERE sessionKey = ? " session-key)))

(defn check-session-key
  [session-key]
  (let [session (get-session-by-session-key session-key)]
    (:active session)))

(defn fail-login
  []
  (fail "Invalid login credentials"))

(defn login
  [username password]
  (let [user (get-user-by-username username)]
    (cond
     (not user) (fail-login)
     (not (check-password user password)) (fail-login)
     :else (pass {:username (:username user) :sessionKey (create-user-session user)}))))

(defn logout
  [session-key]
  (update-record-by-column 
    "UserSession"
    {:active false :dateLoggedOut (get-current-timestamp)}
    "sessionKey" 
    session-key)
  (pass {}))

(defn get-user-by-session-key
  [session-key]
  (get-record-by-id "Staff" (:staff_id (get-session-by-session-key session-key))))

(defn update-password
  [sessionKey newPassword]
  (let [user-id (:staff_id (get-session-by-session-key sessionKey))]
    (update-record "Staff" {:password newPassword} user-id)))

(defn change-password
  [old-password new-password new-confirmation session-key]
  (cond
   (not old-password) (fail "You must provide the old password.")
   (not new-password) (fail "You must provide new password.")
   (not= new-password new-confirmation) (fail "Passwords do not match")
   (not (check-password  (get-user-by-session-key session-key) old-password)) (fail "Incorrect old password")
   :else
   (pass (update-password session-key new-password))))

(defn- user-dto 
  [user]
  (merge user {:fullname (str (:firstname user) " " (:lastname user))}))

(defn load-all-users
  []
  (pass (map user-dto (get-all-records "Staff"))))

